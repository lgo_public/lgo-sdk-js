import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import {
  AxiosMock,
  createApiUserOperation,
  createUserOperation
} from '../../../test';
import { decomposeUrl } from '../../../tools';
import { createGetMyOperations, GetMyOperations } from './getMyOperations';
import { OperationType } from './userOperations';

describe('Get my operations', () => {
  let axios: AxiosInstance;
  let getMyOperations: GetMyOperations;

  beforeEach(() => {
    const configuration = {
      accountingUrl: 'http://accounting'
    } as Configuration;
    axios = new AxiosMock();
    getMyOperations = createGetMyOperations({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        result: {
          operations: [
            createApiUserOperation({ id: '1' }),
            createApiUserOperation({ id: '2' })
          ]
        },
        next_page: 'page42'
      }
    });
  });

  it('should get corresponding resource', async () => {
    await getMyOperations();

    const url = (axios.get as any).mock.calls[0][0];
    expect(url).toContain('http://accounting/operations');
  });

  it('should use provided query to get resource', async () => {
    await getMyOperations({
      currencyCode: 'BTC',
      type: OperationType.deposit,
      maxResults: 3,
      page: '42'
    });

    const url = (axios.get as any).mock.calls[0][0];
    const { baseUrl, params } = decomposeUrl(url);
    expect(baseUrl).toEqual('http://accounting/operations');
    expect(params).toEqual({
      currency_code: 'BTC',
      type: OperationType.deposit,
      max_results: '3',
      page: '42'
    });
  });

  it('should return page', async () => {
    const result = await getMyOperations({
      currencyCode: 'BTC',
      type: OperationType.deposit
    });

    expect(result).toEqual({
      results: [
        createUserOperation({ id: '1' }),
        createUserOperation({ id: '2' })
      ],
      nextPage: 'page42'
    });
  });

  it('should reject if currency code is malformed', async () => {
    const call = getMyOperations({
      currencyCode: 3
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `currencyCode` to be of type `string` but received type `number`'
    );
  });

  it('should reject if type is malformed', async () => {
    const call = getMyOperations({
      type: 3
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `type` to be of type `string` but received type `number`'
    );
  });

  it('should reject if pagination is malformed', async () => {
    const call = getMyOperations({
      currencyCode: 'BTC',
      type: OperationType.deposit,
      maxResults: 'help me'
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `maxResults` to be of type `number` but received type `string`'
    );
  });
});
