import { RSA_PKCS1_OAEP_PADDING } from 'constants';
import { publicEncrypt } from 'crypto';

import { Key } from '../key';
import { orderCancellationToCSV, orderToCSV } from './orderSerialization';
import { OrderToPlace } from './types';

export type EncryptOrder = (
  key: Key,
  order: OrderToPlace,
  timestamp: number
) => Promise<string>;

export const encryptOrder: EncryptOrder = async (
  key: Key,
  order: OrderToPlace,
  timestamp: number
): Promise<string> => {
  const serializedOrder = orderToCSV(order, timestamp);
  return encrypt(key.publicKey, serializedOrder);
};

export type EncryptOrderCancellation = (
  key: Key,
  orderId: string,
  timestamp: number
) => Promise<string>;

export const encryptOrderCancellation: EncryptOrderCancellation = async (
  key: Key,
  orderId: string,
  timestamp: number
): Promise<string> => {
  const serializedCancelOrder = orderCancellationToCSV(orderId, timestamp);
  return encrypt(key.publicKey, serializedCancelOrder);
};

function encrypt(key: string, message: string): string {
  const keyObject = {
    key,
    padding: RSA_PKCS1_OAEP_PADDING
  };
  return publicEncrypt(keyObject, Buffer.from(message)).toString('base64');
}
