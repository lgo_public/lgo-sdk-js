import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import { AxiosMock, samples } from '../../../test';
import { createGetCandles, GetCandles } from './getCandles';

describe('Get candles', () => {
  let axios: AxiosInstance;
  let getCandles: GetCandles;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getCandles = createGetCandles({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        prices: [
          [
            samples.mayIso8601,
            '7000.1',
            '8000.1',
            '7500.1',
            '7600.1',
            '10000.1'
          ]
        ]
      }
    });
  });

  it('should use get corresponding resource', async () => {
    await getCandles({
      productId: 'BTC-USD',
      start: samples.april,
      end: samples.june,
      granularity: 3600
    });

    const start = encodeURIComponent(samples.aprilIso8601);
    const end = encodeURIComponent(samples.juneIso8601);
    const expectedUrl = `http://exchange/v1/history/products/BTC-USD/candles?start=${start}&end=${end}&granularity=3600`;
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return candles', async () => {
    const result = await getCandles({
      productId: 'BTC-USD',
      start: samples.april,
      end: samples.june,
      granularity: 3600
    });

    expect(result).toEqual({
      prices: [
        {
          time: samples.may,
          low: 7000.1,
          high: 8000.1,
          open: 7500.1,
          close: 7600.1,
          volume: 10000.1
        }
      ]
    });
  });

  it('should reject if productId is malformed', async () => {
    const call = getCandles({
      productId: 3,
      start: samples.april,
      end: samples.june,
      granularity: 3600
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });

  it('should reject if start is malformed', async () => {
    const call = getCandles({
      productId: 'BTC-USD',
      start: 3,
      end: samples.june,
      granularity: 3600
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `start` to be of type `date` but received type `number`'
    );
  });

  it('should reject if end is malformed', async () => {
    const call = getCandles({
      productId: 'BTC-USD',
      start: samples.april,
      end: 3,
      granularity: 3600
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `end` to be of type `date` but received type `number`'
    );
  });

  it('should reject if granularity is malformed', async () => {
    const call = getCandles({
      productId: 'BTC-USD',
      start: samples.april,
      end: samples.june,
      granularity: 'hello'
    } as any);

    await expect(call).rejects.toThrow(
      'Expected property `granularity` to be of type `number` but received type `string`'
    );
  });
});
