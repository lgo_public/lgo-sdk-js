import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import { AxiosMock } from '../../../test';
import { createGetProducts, GetProducts } from './getProducts';

describe('Get products', () => {
  let axios: AxiosInstance;
  let getProducts: GetProducts;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getProducts = createGetProducts({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        products: [
          {
            id: 'BTC-USD',
            base: { id: 'BTC', limits: { min: '0.001', max: '1000' } },
            quote: {
              id: 'USD',
              increment: '0.01',
              limits: { min: '10', max: '1000000' }
            },
            total: { limits: { min: '10', max: '50000000' } }
          }
        ]
      }
    });
  });

  it('should get corresponding resource', async () => {
    await getProducts();

    const expectedUrl = 'http://exchange/v1/live/products';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return products', async () => {
    const result = await getProducts();

    expect(result).toEqual({
      products: [
        {
          id: 'BTC-USD',
          base: { id: 'BTC', limits: { min: 0.001, max: 1000 } },
          quote: {
            id: 'USD',
            increment: 0.01,
            limits: { min: 10, max: 1000000 }
          },
          total: { limits: { min: 10, max: 50000000 } }
        }
      ]
    });
  });
});
