import { AxiosInstance } from 'axios';
import * as moment from 'moment';
import ow from 'ow';

import { Configuration } from '../../../configuration';
import { createUrl } from '../../../tools';
import { ApiCandles, Candles, convertApiCandles } from './candles';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetCandlesQuery {
  productId: string;
  start: Date;
  end: Date;
  granularity: number;
}

export type GetCandles = (query: GetCandlesQuery) => Promise<Candles>;

export function createGetCandles(dependencies: Dependencies): GetCandles {
  const { axios, configuration } = dependencies;

  return async (query: GetCandlesQuery) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        productId: ow.string,
        start: ow.date,
        end: ow.date,
        granularity: ow.number
      })
    );
    const { productId, start, end, granularity } = query;
    const url = createUrl(
      `${configuration.exchangeUrl}/v1/history/products/${productId}/candles`,
      {
        start: moment(start)
          .utc()
          .toISOString(),
        end: moment(end)
          .utc()
          .toISOString(),
        granularity
      }
    );
    const { data } = await axios.get<ApiCandles>(url);
    return convertApiCandles(data);
  };
}
