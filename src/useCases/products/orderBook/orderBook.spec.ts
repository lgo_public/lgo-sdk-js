import {
  ApiMarketDataL1,
  ApiMarketDataL2,
  convertApiMarketDataL1,
  convertApiMarketDataL2
} from './orderBook';

describe('Market data', () => {
  describe('while converting api market data level 1', () => {
    it('should return data', () => {
      const apiData: ApiMarketDataL1 = {
        product_id: 'BTC-USD',
        bids: [['5.7000', '1.00000000']],
        asks: [['6.1000', '4.70000000']]
      };

      const data = convertApiMarketDataL1(apiData);

      expect(data).toEqual({
        productId: 'BTC-USD',
        bestBid: { price: 5.7, quantity: 1 },
        bestAsk: { price: 6.1, quantity: 4.7 }
      });
    });

    it('should return default values when ask or bid is missing', () => {
      const apiData: ApiMarketDataL1 = {
        product_id: 'BTC-USD',
        bids: [],
        asks: []
      };

      const data = convertApiMarketDataL1(apiData);

      expect(data).toEqual({
        productId: 'BTC-USD',
        bestBid: { price: 0, quantity: 0 },
        bestAsk: { price: 0, quantity: 0 }
      });
    });
  });

  describe('while converting api market data level 2', () => {
    it('should return data', () => {
      const apiData: ApiMarketDataL2 = {
        product_id: 'BTC-USD',
        bids: [['5.7', '1'], ['1.3', '5.8']],
        asks: [['6.1', '4.7'], ['6.3', '3.3']]
      };

      const data = convertApiMarketDataL2(apiData);

      expect(data).toEqual({
        productId: 'BTC-USD',
        bids: [{ price: 5.7, quantity: 1 }, { price: 1.3, quantity: 5.8 }],
        asks: [{ price: 6.1, quantity: 4.7 }, { price: 6.3, quantity: 3.3 }]
      });
    });
  });
});
