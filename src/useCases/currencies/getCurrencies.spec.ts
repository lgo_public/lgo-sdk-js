import { AxiosInstance } from 'axios';

import { Configuration } from '../../configuration';
import { AxiosMock } from '../../test';
import { createGetCurrencies, GetCurrencies } from './getCurrencies';

describe('Get currencies', () => {
  let axios: AxiosInstance;
  let getCurrencies: GetCurrencies;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getCurrencies = createGetCurrencies({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        currencies: [
          { name: 'Bitcoin', code: 'BTC', decimals: 8 },
          { name: 'United States Dollar', code: 'USD', decimals: 4 }
        ]
      }
    });
  });

  it('should get corresponding resource', async () => {
    await getCurrencies();

    const expectedUrl = 'http://exchange/v1/live/currencies';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return currencies', async () => {
    const result = await getCurrencies();

    expect(result).toEqual({
      currencies: [
        { name: 'Bitcoin', code: 'BTC', decimals: 8 },
        { name: 'United States Dollar', code: 'USD', decimals: 4 }
      ]
    });
  });
});
