import { first } from 'lodash/fp';

export interface ApiMarketDataL1 {
  product_id: string;
  bids: ApiMarketDataQuote[];
  asks: ApiMarketDataQuote[];
}

export type ApiMarketDataQuote = [string, string];

export interface MarketDataL1 {
  productId: string;
  bestBid: MarketDataQuote;
  bestAsk: MarketDataQuote;
}

export interface ApiMarketDataL2 {
  product_id: string;
  bids: Array<[string, string]>;
  asks: Array<[string, string]>;
}

export interface MarketDataL2 {
  productId: string;
  bids: MarketDataQuote[];
  asks: MarketDataQuote[];
}

export interface MarketDataQuote {
  price: number;
  quantity: number;
}

export function convertApiMarketDataL1(
  apiMarketDataL1: ApiMarketDataL1
): MarketDataL1 {
  const { product_id, bids, asks } = apiMarketDataL1;
  return {
    productId: product_id,
    bestBid: convertApiMarketDataQuote(first(bids)),
    bestAsk: convertApiMarketDataQuote(first(asks))
  };
}

export function convertApiMarketDataL2(apiMarketDataL2: ApiMarketDataL2) {
  const { product_id, bids, asks } = apiMarketDataL2;
  return {
    productId: product_id,
    bids: bids.map(convertApiMarketDataQuote),
    asks: asks.map(convertApiMarketDataQuote)
  };
}

function convertApiMarketDataQuote(
  apiMarketDataQuote: ApiMarketDataQuote | undefined
): MarketDataQuote {
  if (!apiMarketDataQuote) {
    return { price: 0, quantity: 0 };
  }
  const [price, quantity] = apiMarketDataQuote;
  return {
    price: Number.parseFloat(price),
    quantity: Number.parseFloat(quantity)
  };
}
