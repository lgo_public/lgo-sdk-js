import { AxiosInstance } from 'axios';

import { Configuration } from '../../configuration';
import { ApiCurrencies, convertApiCurrencies, Currencies } from './currencies';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export type GetCurrencies = () => Promise<Currencies>;

export function createGetCurrencies(dependencies: Dependencies): GetCurrencies {
  const { axios, configuration } = dependencies;

  return async () => {
    const { data } = await axios.get<ApiCurrencies>(
      `${configuration.exchangeUrl}/v1/live/currencies`
    );
    return convertApiCurrencies(data);
  };
}
