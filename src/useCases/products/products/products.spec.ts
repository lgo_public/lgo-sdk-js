import { convertApiProducts } from './products';

describe('Products', () => {
  describe('while converting api products', () => {
    it('should return products', async () => {
      const apiProducts = {
        products: [
          {
            id: 'BTC-USD',
            base: { id: 'BTC', limits: { min: '0.001', max: '1000' } },
            quote: {
              id: 'USD',
              increment: '0.01',
              limits: { min: '10', max: '1000000' }
            },
            total: { limits: { min: '10', max: '50000000' } }
          }
        ]
      };

      const products = convertApiProducts(apiProducts);

      expect(products).toEqual({
        products: [
          {
            id: 'BTC-USD',
            base: { id: 'BTC', limits: { min: 0.001, max: 1000 } },
            quote: {
              id: 'USD',
              increment: 0.01,
              limits: { min: 10, max: 1000000 }
            },
            total: { limits: { min: 10, max: 50000000 } }
          }
        ]
      });
    });
  });
});
