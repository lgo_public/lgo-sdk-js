import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../configuration';
import {
  convertPaginationQueryToApi,
  createUrl,
  PaginationQuery,
  paginationShape
} from '../../../tools';
import {
  ApiProductTradePage,
  convertApiProductTradePage,
  ProductTradePage
} from './productTrades';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetTradesQuery extends PaginationQuery {
  productId: string;
}

export type GetProductTrades = (
  query: GetTradesQuery
) => Promise<ProductTradePage>;

export function createGetProductTrades(
  dependencies: Dependencies
): GetProductTrades {
  const { axios, configuration } = dependencies;

  return async (query: GetTradesQuery) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        ...paginationShape,
        productId: ow.string
      })
    );
    const apiQuery = convertPaginationQueryToApi(query);
    const { productId } = query;
    const { data } = await axios.get<ApiProductTradePage>(
      createUrl(
        `${configuration.exchangeUrl}/v1/history/products/${productId}/trades`,
        apiQuery
      )
    );
    return convertApiProductTradePage(data);
  };
}
