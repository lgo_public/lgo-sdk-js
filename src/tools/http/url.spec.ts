import { createUrl, decomposeUrl } from './url';

describe('Url', () => {
  describe('on creating url', () => {
    it('should format url base url and params', () => {
      const result = createUrl('google.fr', { query: 'test' });

      expect(result).toEqual('google.fr?query=test');
    });

    it('should ignore params when missing', () => {
      expect(createUrl('google.fr', {})).toEqual('google.fr');
      expect(createUrl('google.fr')).toEqual('google.fr');
    });

    it('should ignore undefined or null params values', () => {
      expect(createUrl('google.fr', { test: undefined })).toEqual('google.fr');
      expect(createUrl('google.fr', { test: null })).toEqual('google.fr');
      expect(
        createUrl('google.fr', { toIgnore: undefined, tokeep: 'ok' })
      ).toEqual('google.fr?tokeep=ok');
    });

    it("won't ignore 0 as params values", () => {
      expect(createUrl('google.fr', { tokeep: 0 })).toEqual(
        'google.fr?tokeep=0'
      );
    });
  });

  describe('on decomposing', () => {
    it('should return base url and query params', () => {
      const result = decomposeUrl('google.fr?query=test&sort=asc');

      expect(result).toEqual({
        baseUrl: 'google.fr',
        params: { query: 'test', sort: 'asc' }
      });
    });

    it('should return no query params if not any', () => {
      const result = decomposeUrl('google.fr');

      expect(result).toEqual({
        baseUrl: 'google.fr',
        params: {}
      });
    });
  });
});
