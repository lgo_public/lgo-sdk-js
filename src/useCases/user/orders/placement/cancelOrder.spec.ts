import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import { PrepareOrderCancellation } from '../../../../domain';
import { AxiosMock, createKey } from '../../../../test';
import { CancelOrder, createCancelOrder } from './cancelOrder';

describe('Cancel order', () => {
  let axios: AxiosInstance;
  let cancelOrder: CancelOrder;
  let prepareOrderCancellation: PrepareOrderCancellation;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    prepareOrderCancellation = jest.fn().mockResolvedValue(undefined);
    axios.post = jest.fn().mockResolvedValue({});
    axios.delete = jest.fn().mockResolvedValue({});
    cancelOrder = createCancelOrder({
      configuration,
      axios,
      prepareOrderCancellation
    });
  });

  describe('encrypted', () => {
    it('should prepare cancel order then post it to corresponding resource', async () => {
      const key = createKey({ id: '1337' });
      (prepareOrderCancellation as any).mockResolvedValue({
        keyId: '1337',
        order: 'encrypted',
        signature: { source: 'RSA', value: 'signed' },
        reference: 42
      });

      await cancelOrder({
        key,
        reference: 42,
        orderId: '5'
      });

      const expectedUrl = 'http://exchange/v1/live/orders/encrypted';
      const expectedBody = {
        key_id: '1337',
        order: 'encrypted',
        signature: {
          source: 'RSA',
          value: 'signed'
        },
        reference: 42
      };
      expect(axios.post).toHaveBeenCalledWith(expectedUrl, expectedBody);
    });

    it('should reject if cancellation is malformed', async () => {
      const key = createKey({ id: '1337' });

      const call = cancelOrder({
        key,
        reference: 42
      } as any);

      await expect(call).rejects.toThrow(
        'Expected property `orderId` to be of type `string` but received type `undefined`'
      );
    });
  });

  describe('unencrypted', () => {
    it('should send delete order to corresponding resource', async () => {
      await cancelOrder({ orderId: '5' });
      const expectedUrl = 'http://exchange/v1/live/orders/5';
      expect(axios.delete).toHaveBeenCalledWith(expectedUrl);
    });
  });
});
