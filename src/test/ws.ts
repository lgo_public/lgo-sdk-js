import { WebSocketClient } from '../tools';
import { withEmitter } from './emitter';
import { mock } from './mock';

export const WsMock = mock<WebSocketClient>(() =>
  withEmitter({
    connect: jest.fn().mockResolvedValue(undefined),
    disconnect: jest.fn().mockReturnValue(undefined),
    send: jest.fn().mockResolvedValue(undefined)
  })
);
