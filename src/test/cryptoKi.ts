import { CryptoKi } from '../tools';
import { mock } from './mock';

export const CryptoKiMock = mock<CryptoKi>(() => ({
  sign: jest
    .fn()
    .mockImplementation(message => Promise.resolve(`${message}|signed`))
}));
