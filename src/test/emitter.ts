import { EventEmitter } from 'events';
import { assignIn } from 'lodash/fp';

export function withEmitter<T>(object: T): T {
  const eventEmitter = new EventEmitter();
  return assignIn(eventEmitter, object);
}
