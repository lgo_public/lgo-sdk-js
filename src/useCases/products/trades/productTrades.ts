import * as moment from 'moment';

import { OrderSide } from '../../../domain';
import { ApiEntityPage, convertApiPage, EntityPage } from '../../../tools';

export interface ApiProductTrade {
  id: string;
  order_buyer_id: string;
  order_seller_id: string;
  product_id: string;
  price: string;
  quantity: string;
  creation_date: string;
  side: OrderSide;
  buyer_fee: string;
  seller_fee: string;
}

export interface ApiProductTradePage extends ApiEntityPage<ApiProductTrade> {
  result: {
    trades: ApiProductTrade[];
  };
}

export interface ProductTrade {
  id: string;
  orderBuyerId: string;
  orderSellerId: string;
  productId: string;
  price: number;
  quantity: number;
  creationDate: Date;
  side: OrderSide;
  buyerFee: number;
  sellerFee: number;
}

export type ProductTradePage = EntityPage<ProductTrade>;

export function convertApiProductTradePage(
  apiProductTradePage: ApiProductTradePage
): ProductTradePage {
  return convertApiPage(
    apiProductTradePage,
    p => p.result.trades,
    convertApiProductTrade
  );
}

function convertApiProductTrade(
  apiProductTrade: ApiProductTrade
): ProductTrade {
  return {
    id: apiProductTrade.id,
    orderBuyerId: apiProductTrade.order_buyer_id,
    orderSellerId: apiProductTrade.order_seller_id,
    productId: apiProductTrade.product_id,
    quantity: Number.parseFloat(apiProductTrade.quantity),
    price: Number.parseFloat(apiProductTrade.price),
    creationDate: moment.utc(apiProductTrade.creation_date).toDate(),
    side: apiProductTrade.side,
    buyerFee: Number.parseFloat(apiProductTrade.buyer_fee),
    sellerFee: Number.parseFloat(apiProductTrade.seller_fee)
  };
}
