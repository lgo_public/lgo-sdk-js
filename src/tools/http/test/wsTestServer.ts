import { IncomingHttpHeaders, IncomingMessage } from 'http';
import * as WebSocket from 'ws';
import { wait } from '../../../test';

export class WsTestServer {
  public connectionReceivedCount: number;
  public pingReceivedTimes: number[];
  public instance: WebSocket.Server | null;
  public connectedSocket: WebSocket | null;
  private shouldRejectClients: boolean;
  private shouldTimeoutConnections: boolean;
  private readonly port: number;
  public clientHeaders: IncomingHttpHeaders;

  constructor(port: number) {
    this.port = port;
    this.connectionReceivedCount = 0;
    this.pingReceivedTimes = [];
    this.connectedSocket = null;
    this.shouldRejectClients = false;
    this.shouldTimeoutConnections = false;
    this.clientHeaders = {};
    this.instance = null;
  }

  public start() {
    return new Promise((resolve: () => void) => {
      this.instance = new WebSocket.Server(
        {
          port: this.port,
          verifyClient: this.verifyClient.bind(this)
        },
        resolve
      );
      this.connectedSocket = null;
      this.instance.on('connection', (ws: WebSocket) => {
        this.connectionReceivedCount++;
        this.connectedSocket = ws;
        ws.on('ping', () => {
          this.pingReceivedTimes.push(Date.now());
        });
      });
    });
  }

  public rejectClients() {
    this.shouldRejectClients = true;
  }

  public timeoutConnections() {
    this.shouldTimeoutConnections = true;
  }

  public neverPong() {
    if (this.connectedSocket) {
      this.connectedSocket.pong = () => undefined;
    }
  }

  private verifyClient(info: { req: IncomingMessage }, cb: any) {
    this.clientHeaders = info.req.headers;
    if (this.shouldRejectClients) {
      cb(false, 401, 'bleh', {});
    } else if (this.shouldTimeoutConnections) {
      wait(2000).then(() => cb(true));
    } else {
      cb(true);
    }
  }

  public closeClient() {
    if (this.connectedSocket) {
      this.connectedSocket.close();
    }
  }

  public stop() {
    return new Promise(resolve => {
      if (this.instance) {
        this.instance.close(() => resolve());
        this.instance = null;
      }
    });
  }
}
