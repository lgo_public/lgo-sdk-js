import { IncomingMessage } from 'http';
import { getOr } from 'lodash/fp';

import { ApiError, ErrorCode } from '../apiError';

const httpCodeToApiCode: { [code: number]: ErrorCode | undefined } = {
  401: ErrorCode.unauthorized,
  404: ErrorCode.notFound,
  500: ErrorCode.unknown
};

export function wrapAxiosError(wrapped: Error): ApiError {
  const httpCode = getOr(undefined, ['response', 'status'], wrapped);
  const defaultCode = httpCodeToApiCode[httpCode] || ErrorCode.unknown;
  const apiCode = getOr(defaultCode, ['response', 'data', 'code'], wrapped);
  return new ApiError('Request failed', { code: apiCode, httpCode });
}

export function wrapNodeResponseError(wrapped: IncomingMessage): ApiError {
  const httpCode = wrapped.statusCode || 500;
  const code = httpCodeToApiCode[httpCode] || ErrorCode.unknown;
  return new ApiError('Request failed', { code, httpCode });
}
