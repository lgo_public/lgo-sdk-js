import ow from 'ow';

import { CreateUtcDate, CryptoKi } from '../../tools';
import { CancellationPrepared, OrderCancellation } from './types';

interface Dependencies {
  createUtcDate: CreateUtcDate;
  cryptoKi: CryptoKi;
}

export type PrepareUnencryptedOrderCancellation = (
  cancellation: OrderCancellation
) => Promise<CancellationPrepared>;

export function createPrepareUnencryptedOrderCancellation(
  dependencies: Dependencies
): PrepareUnencryptedOrderCancellation {
  const { createUtcDate, cryptoKi } = dependencies;

  return async (cancellation: OrderCancellation) => {
    const { reference, orderId } = cancellation;
    const timestamp = Math.floor(createUtcDate().getTime() / 1000);
    const cancel = {
      order_id: orderId,
      timestamp
    };
    const signedCancellation = await cryptoKi.sign(JSON.stringify(cancel));
    const signature = {
      source: 'RSA',
      value: signedCancellation
    };
    if (reference) {
      return { ...cancel, signature, reference };
    } else {
      return { ...cancel, signature };
    }
  };
}

export function validateUnencryptedOrderCancellation(
  cancellation: OrderCancellation
): void {
  ow(
    cancellation,
    'cancellation',
    ow.object.exactShape({
      reference: ow.optional.number,
      orderId: ow.string
    })
  );
}
