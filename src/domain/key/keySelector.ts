import { maxBy } from 'lodash/fp';
import ow from 'ow';

import { CreateUtcDate } from '../../tools';
import { Key, keyShape } from './key';

export type SelectKey = (keys: Key[]) => Key | undefined;

interface Dependencies {
  createUtcDate: CreateUtcDate;
}

export function createSelectKey(dependencies: Dependencies) {
  const { createUtcDate } = dependencies;
  return (keys: Key[]) => {
    ow(keys, 'keys', ow.array.ofType(ow.object.exactShape(keyShape)));
    const now = createUtcDate();
    const currents = keys.filter(k => k.enabledAt <= now && now < k.disabledAt);
    return selectKey(currents, now);
  };
}

const selectKey = (keys: Key[], now: Date) =>
  maxBy((key: Key) =>
    Math.min(
      now.getTime() - key.enabledAt.getTime(),
      key.disabledAt.getTime() - now.getTime()
    )
  )(keys);
