import * as moment from 'moment';

import { OrderSide, OrderStatus, OrderType } from '../../../../domain';
import { ApiEntityPage, convertApiPage, EntityPage } from '../../../../tools';

export interface ApiUserOrder {
  id: string;
  batch_id: string;
  type: OrderType;
  side: OrderSide;
  product_id: string;
  quantity: string;
  price: string | null;
  creation_date: string;
  remaining_quantity: string;
  status: OrderStatus;
}

export interface ApiUserOrderPage extends ApiEntityPage<ApiUserOrder> {
  result: {
    orders: ApiUserOrder[];
  };
}

export interface LiveApiUserOrders {
  batch_id: string;
  orders: LiveApiUserOrder[];
}

export interface UserOrder {
  id: string;
  type: OrderType;
  side: OrderSide;
  productId: string;
  quantity: number;
  remainingQuantity: number;
  price?: number;
  creationDate: Date;
  status: OrderStatus;
}

export interface LiveApiUserOrder {
  order_id: string;
  order_type: OrderType;
  quantity: string;
  price: string | null;
  remaining_quantity: string;
  side: OrderSide;
  order_creation_time: string;
}

export interface LiveOrders {
  batchId: string;
  orders: UserOrder[];
}

export type UserOrderPage = EntityPage<UserOrder>;

export function convertApiUserOrderPage(
  apiUserOrderPage: ApiUserOrderPage
): UserOrderPage {
  return convertApiPage(
    apiUserOrderPage,
    p => p.result.orders,
    convertApiUserOrder
  );
}

export function convertLiveApiUserOrders(liveApiUserOrders: LiveApiUserOrders, productId: string) {
  return {
    batchId: liveApiUserOrders.batch_id,
    orders: liveApiUserOrders.orders.map((o) => convertLiveApiUserOrder(o, productId))
  };
}

function convertLiveApiUserOrder(liveApiUserOrder: LiveApiUserOrder, productId: string) {
  return {
    id: liveApiUserOrder.order_id,
    type: liveApiUserOrder.order_type,
    side: liveApiUserOrder.side,
    status: OrderStatus.open,
    quantity: Number.parseFloat(liveApiUserOrder.quantity),
    remainingQuantity: Number.parseFloat(liveApiUserOrder.remaining_quantity),
    price:
      liveApiUserOrder.price === null
        ? undefined
        : Number.parseFloat(liveApiUserOrder.price),
    creationDate: moment.utc(liveApiUserOrder.order_creation_time).toDate(),
    productId
  };
}

export function convertApiUserOrder(apiUserOrder: ApiUserOrder): UserOrder {
  return {
    id: apiUserOrder.id,
    type: apiUserOrder.type,
    side: apiUserOrder.side,
    status: apiUserOrder.status,
    productId: apiUserOrder.product_id,
    quantity: Number.parseFloat(apiUserOrder.quantity),
    remainingQuantity: Number.parseFloat(apiUserOrder.remaining_quantity),
    price:
      apiUserOrder.price === null
        ? undefined
        : Number.parseFloat(apiUserOrder.price),
    creationDate: moment.utc(apiUserOrder.creation_date).toDate()
  };
}
