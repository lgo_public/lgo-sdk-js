import { orderCancellationToCSV, orderToCSV } from './orderSerialization';
import { OrderSide, OrderToPlace, OrderType } from './types';

const timestamp = Date.now();

describe('Order serialization', () => {
  describe('while serializing order to csv', () => {
    it('could handle a limit order', () => {
      const order: OrderToPlace = {
        type: OrderType.limit,
        side: OrderSide.sell,
        productId: 'BTC-USD',
        quantity: 1,
        price: 10
      };

      const result = orderToCSV(order, timestamp);

      expect(result).toEqual(`L,S,BTC-USD,1,10,gtc,${timestamp}`);
    });

    it('could handle a market order', () => {
      const order: OrderToPlace = {
        type: OrderType.market,
        side: OrderSide.buy,
        productId: 'BTC-USD',
        quantity: 8
      };

      const result = orderToCSV(order, timestamp);

      expect(result).toEqual(`M,B,BTC-USD,8,,,${timestamp}`);
    });
  });

  describe('while serializing order cancellation to csv', () => {
    it('should do the job', () => {
      const result = orderCancellationToCSV('1234', timestamp);

      expect(result).toEqual(`C,1234,${timestamp}`);
    });
  });
});
