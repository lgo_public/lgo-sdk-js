import * as moment from 'moment';

export type CreateUtcDate = () => Date;

export function createUtcDate(): Date {
  return moment.utc().toDate();
}
