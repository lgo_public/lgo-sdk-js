import { WebSocketClient, WebSocketClientCreation } from '../webSocketClient';

export class WsTestClient {
  public openReceivedCount: number;
  public messagesReceived: any[];
  public errorsReceived: Error[];
  public willCloseReceivedCount: number;
  public closedReceivedCount: number;
  public pongReceivedTimes: number[];
  public instance: WebSocketClient;

  constructor(creation: WebSocketClientCreation) {
    this.openReceivedCount = 0;
    this.messagesReceived = [];
    this.errorsReceived = [];
    this.willCloseReceivedCount = 0;
    this.closedReceivedCount = 0;
    this.pongReceivedTimes = [];
    this.instance = this.createClient(creation);
  }

  private createClient(creation: WebSocketClientCreation): WebSocketClient {
    const result = new WebSocketClient(creation);
    result.on('error', e => this.errorsReceived.push(e));
    result.on('message', m => this.messagesReceived.push(m));
    result.on('open', () => this.openReceivedCount++);
    result.on('pong', () => {
      this.pongReceivedTimes.push(Date.now());
    });
    result.on('will-close', () => {
      this.willCloseReceivedCount++;
    });
    result.on('close', () => {
      this.closedReceivedCount++;
    });
    return result;
  }
}
