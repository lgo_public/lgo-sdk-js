import { AxiosInstance } from 'axios';
import * as moment from 'moment';

import { Configuration } from '../../configuration';
import { Key, KeySummary } from '../../domain';
import { CreateUtcDate } from '../../tools';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
  createUtcDate: CreateUtcDate;
}

export type GetKeys = () => Promise<Key[]>;

export function createGetKeys(dependencies: Dependencies): GetKeys {
  const { configuration, createUtcDate, axios } = dependencies;
  return async () => {
    try {
      const now = createUtcDate();
      const index = await downloadIndex();
      const notExpiredSummaries = filterNotExpiredSummaries(index, now);
      const keys = await downloadPublicKeys(notExpiredSummaries);
      return keys;
    } catch (error) {
      throw new Error(`Impossible to download keys: ${error.message}`);
    }
  };

  async function downloadIndex(): Promise<KeySummary[]> {
    try {
      const url = `${configuration.keysUrl}/index.txt`;
      const { data } = await axios.get<string>(url);
      return parseSummaries(data);
    } catch (error) {
      throw new Error(`Impossible to download index: ${error.message}`);
    }
  }

  function parseSummaries(text: string): KeySummary[] {
    return text.split('\n').reduce(
      (result, row) => {
        const summary = parseSummary(row);
        if (summary) {
          result.push(summary);
        }
        return result;
      },
      [] as KeySummary[]
    );
  }

  function parseSummary(line: string): KeySummary | undefined {
    const columns = line.split(' ');
    if (columns.length < 3) {
      return undefined;
    }
    return {
      id: columns[0],
      enabledAt: moment.utc(columns[1]).toDate(),
      disabledAt: moment.utc(columns[2]).toDate()
    };
  }

  function filterNotExpiredSummaries(
    index: KeySummary[],
    now: Date
  ): KeySummary[] {
    return index.filter(k => now < k.disabledAt);
  }

  async function downloadPublicKeys(summaries: KeySummary[]) {
    const result: Key[] = [];
    for (const summary of summaries) {
      const key = await combineWithPublicKey(summary);
      result.push(key);
    }
    return result;
  }

  async function combineWithPublicKey(summary: KeySummary): Promise<Key> {
    try {
      const url = `${configuration.keysUrl}/${summary.id}_public.pem`;
      const { data } = await axios.get<string>(url);
      return Object.assign({}, summary, { publicKey: data });
    } catch (error) {
      throw new Error(`Impossible to download public key: ${error.message}`);
    }
  }
}
