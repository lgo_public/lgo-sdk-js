import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import {
  AxiosMock,
  createApiUserOrder,
  createUserOrder
} from '../../../../test';
import { createGetMyOrder, GetMyOrder } from './getMyOrder';

describe('Get my order', () => {
  let axios: AxiosInstance;
  let getMyOrder: GetMyOrder;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getMyOrder = createGetMyOrder({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: createApiUserOrder({ id: '1' })
    });
  });

  it('should get corresponding resource', async () => {
    await getMyOrder({ id: '1' });

    const expectedUrl = 'http://exchange/v1/history/orders/1';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return order', async () => {
    const result = await getMyOrder({ id: '1' });

    expect(result).toEqual(createUserOrder({ id: '1' }));
  });

  it('should reject if product id is malformed', async () => {
    const call = getMyOrder({ id: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `id` to be of type `string` but received type `number`'
    );
  });
});
