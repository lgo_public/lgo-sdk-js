import ow from 'ow';
import { CreateUtcDate, CryptoKi } from '../../tools';

import { OrderToPlace, UnEncryptedOrderPrepared } from './types';

interface Dependencies {
  createUtcDate: CreateUtcDate;
  cryptoKi: CryptoKi;
}

export type PrepareUnencryptedOrder = (
  placement: OrderToPlace
) => Promise<UnEncryptedOrderPrepared>;

export function createPrepareUnEcryptedOrder(
  dependencies: Dependencies
): PrepareUnencryptedOrder {
  const { createUtcDate, cryptoKi } = dependencies;

  return async (placement: OrderToPlace) => {
    const { price, productId, quantity, reference, side, type } = placement;
    const timestamp = Math.floor(createUtcDate().getTime() / 1000);
    const orderToPlace = price
      ? {
          type,
          side,
          product_id: productId,
          quantity: quantity.toString(),
          price: price.toString(),
          timestamp
        }
      : {
          type,
          side,
          product_id: productId,
          quantity: quantity.toString(),
          timestamp
        };
    const signedOrder = await cryptoKi.sign(JSON.stringify(orderToPlace));
    const signature = {
      source: 'RSA',
      value: signedOrder
    };
    if (reference) {
      return { order: orderToPlace, signature, reference };
    } else {
      return { order: orderToPlace, signature };
    }
  };
}

export function validateUnEncryptedOrderPlacement(
  placement: OrderToPlace
): void {
  ow(
    placement,
    'placement',
    ow.object.exactShape({
      type: ow.string,
      side: ow.string,
      productId: ow.string,
      quantity: ow.number,
      price: ow.optional.number,
      reference: ow.optional.number
    })
  );
}
