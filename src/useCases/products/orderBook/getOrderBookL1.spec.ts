import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import { AxiosMock } from '../../../test';
import { createGetOrderBookL1, GetOrderBookL1 } from './getOrderBookL1';

describe('Get order book level 1', () => {
  let axios: AxiosInstance;
  let getOrderBookL1: GetOrderBookL1;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getOrderBookL1 = createGetOrderBookL1({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        product_id: 'BTC-USD',
        bids: [['5.7000', '1.00000000']],
        asks: [['6.1000', '4.70000000']]
      }
    });
  });

  it('should use get corresponding resource', async () => {
    await getOrderBookL1({ productId: 'BTC-USD' });

    const expectedUrl = 'http://exchange/v1/live/products/BTC-USD/book?level=1';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return products', async () => {
    const result = await getOrderBookL1({ productId: 'BTC-USD' });

    expect(result).toEqual({
      productId: 'BTC-USD',
      bestBid: { price: 5.7, quantity: 1 },
      bestAsk: { price: 6.1, quantity: 4.7 }
    });
  });

  it('should reject if productId is malformed', async () => {
    const call = getOrderBookL1({ productId: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });
});
