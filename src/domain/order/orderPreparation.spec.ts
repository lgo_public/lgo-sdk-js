import {
  createKey,
  createOrderToPlace,
  CryptoKiMock,
  samples
} from '../../test';
import { CreateUtcDate, CryptoKi } from '../../tools';
import { EncryptOrder } from './orderEncryption';
import {
  createPrepareOrder,
  PrepareOrder,
  validateOrderPlacement
} from './orderPreparation';

describe('Order preparation', () => {
  let createUtcDate: CreateUtcDate;
  let prepareOrder: PrepareOrder;
  let cryptoKi: CryptoKi;
  let encryptOrder: EncryptOrder;

  beforeEach(() => {
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    cryptoKi = new CryptoKiMock();
    encryptOrder = jest.fn().mockResolvedValue('encrypted');
    prepareOrder = createPrepareOrder({
      createUtcDate,
      cryptoKi,
      encryptOrder
    });
  });

  it('should encrypt and sign order', async () => {
    const order = createOrderToPlace();
    const key = createKey({ id: '1337' });

    const result = await prepareOrder({
      ...order,
      key,
      reference: 42
    });

    const expectedPreparedOrder = {
      keyId: '1337',
      order: 'encrypted',
      signature: {
        source: 'RSA',
        value: 'encrypted|signed'
      },
      reference: 42
    };
    expect(result).toEqual(expectedPreparedOrder);
  });

  it('should encrypt order using provided key and timestamp', async () => {
    const order = createOrderToPlace();
    const key = createKey({ id: '1337' });

    await prepareOrder({
      ...order,
      key,
      reference: 42
    });

    expect(encryptOrder).toHaveBeenCalledWith(
      key,
      order,
      Math.floor(samples.june.getTime() / 1000)
    );
  });
});

describe('Order prepartion validation', () => {
  it("should fail if order's type is missing", async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace({ type: undefined } as any),
        key,
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `type` to be of type `string` but received type `undefined`'
    );
  });

  it("should fail if order's side is missing", async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace({ side: undefined } as any),
        key,
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `side` to be of type `string` but received type `undefined`'
    );
  });

  it("should fail if order's product id is missing", async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace({ productId: undefined } as any),
        key,
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `productId` to be of type `string` but received type `undefined`'
    );
  });

  it("should fail if order's quantity is missing", async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace({ quantity: undefined } as any),
        key,
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `quantity` to be of type `number` but received type `undefined`'
    );
  });

  it("should fail if order's price is malformed", async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace({ price: 'hello' } as any),
        key,
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `price` to be of type `number` but received type `string`'
    );
  });

  it("won't fail if order's price is missing", async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace({ price: undefined } as any),
        key,
        reference: 42
      } as any);

    await expect(call).not.toThrow();
  });

  it('should fail if reference is malformed', async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderPlacement({
        key,
        ...createOrderToPlace(),
        reference: 'hello'
      } as any);

    await expect(call).toThrow(
      'Expected property `reference` to be of type `number` but received type `string`'
    );
  });

  it('should fail if key is missing', async () => {
    const call = () =>
      validateOrderPlacement({
        ...createOrderToPlace(),
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `key` to exist in object `placement`'
    );
  });
});
