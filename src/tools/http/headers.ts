import { Configuration } from '../../configuration';
import { CryptoKi } from '../cryptoKi';
import { CreateUtcDate } from '../date';

interface Dependencies {
  configuration: Configuration;
  createUtcDate: CreateUtcDate;
  cryptoKi: CryptoKi;
  sdkVersion: string;
}

export type CreateHeaders = (
  targetUrl: string,
  requestBody?: string
) => Promise<Record<string, string>>;

export function createHeadersFactory(
  dependencies: Dependencies
): CreateHeaders {
  const { configuration, createUtcDate, cryptoKi, sdkVersion } = dependencies;
  return async (targetUrl: string, requestBody?: any) => {
    const now = createUtcDate();
    const timestamp = now.getTime();
    const accessKey = configuration.accessKey;
    const urlToSign = createUrlToSign(targetUrl);
    const needToSignedBody =
      requestBody && !targetUrl.match('/v1/live/orders/encrypted');
    const toSign = needToSignedBody
      ? `${timestamp}\n${urlToSign}\n${JSON.stringify(requestBody)}`
      : `${timestamp}\n${urlToSign}`;
    const signature = await cryptoKi.sign(toSign);
    return {
      'X-LGO-DATE': timestamp.toString(),
      'X-LGO-CLIENT-VERSION': sdkVersion,
      Authorization: `LGO ${accessKey}:${signature}`
    };
  };

  function createUrlToSign(url: string) {
    return url
      .replace('http://', '')
      .replace('https://', '')
      .replace('ws://', '')
      .replace('wss://', '');
  }
}
