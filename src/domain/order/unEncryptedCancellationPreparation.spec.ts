import { CryptoKiMock, samples } from '../../test';
import { CreateUtcDate, CryptoKi } from '../../tools';
import {
  createPrepareUnencryptedOrderCancellation,
  PrepareUnencryptedOrderCancellation, validateUnencryptedOrderCancellation
} from './unEncryptedCancellationPreparation';

describe('Unencrypted Order cancellation', () => {
  let createUtcDate: CreateUtcDate;
  let prepareUnencryptedOrderCancellation: PrepareUnencryptedOrderCancellation;
  let cryptoKi: CryptoKi;

  beforeEach(() => {
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    cryptoKi = new CryptoKiMock();
    prepareUnencryptedOrderCancellation = createPrepareUnencryptedOrderCancellation({
      createUtcDate,
      cryptoKi,
    });
  });

  it('should sign order', async () => {
    const result = await prepareUnencryptedOrderCancellation({
      reference: 42,
      orderId: '5'
    });

    const expectedPreparedCancellation = {
      order_id: '5',
      signature: {
        source: 'RSA',
        value: '{"order_id":"5","timestamp":1527811200}|signed'
      },
      timestamp: 1527811200,
      reference: 42
    };
    expect(result).toEqual(expectedPreparedCancellation);
  });

  describe('Unencrypted Order cancellation validation', () => {
    it('should fail if order id is missing', async () => {
      const call = () =>
        validateUnencryptedOrderCancellation({
          reference: 42
        } as any);

      await expect(call).toThrow(
        'Expected property `orderId` to be of type `string` but received type `undefined`'
      );
    });

    it('should fail if reference is malformed', async () => {
      const call = () =>
        validateUnencryptedOrderCancellation({
          orderId: '5',
          reference: 'hello'
        } as any);

      await expect(call).toThrow(
        'Expected property `reference` to be of type `number` but received type `string`'
      );
    });
  });
});
