import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import {
  OrderToPlace,
  PrepareOrder,
  validateOrderPlacement
} from '../../../../domain';
import { convertToPlacedOrder, PlacedOrder } from './placedOrder';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
  prepareOrder: PrepareOrder;
}

export type PlaceOrder = (
  placement: OrderToPlace
) => Promise<PlacedOrder | null>;

export function createPlaceOrder(dependencies: Dependencies): PlaceOrder {
  const { axios, configuration, prepareOrder } = dependencies;

  return async (placement: OrderToPlace) => {
    if (placement.key) {
      validateOrderPlacement(placement);
      const preparation = await prepareOrder(placement);
      if (!preparation) {
        return null;
      }
      const body = {
        key_id: preparation.keyId,
        order: preparation.order,
        signature: preparation.signature,
        reference: preparation.reference
      };
      const { data } = await axios.post(
        `${configuration.exchangeUrl}/v1/live/orders/encrypted`,
        body
      );
      return convertToPlacedOrder(data);
    } else {
      const body = {
        type: placement.type,
        side: placement.side,
        product_id: placement.productId,
        quantity: placement.quantity,
        price: placement.price,
        reference: placement.reference
      };
      const { data } = await axios.post(
        `${configuration.exchangeUrl}/v1/live/orders`,
        body
      );
      return convertToPlacedOrder(data);
    }
  };
}
