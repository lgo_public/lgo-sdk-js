import { Configuration } from '../../configuration';
import { CryptoKiMock, samples } from '../../test';
import { CryptoKi } from '../cryptoKi';
import { CreateHeaders, createHeadersFactory } from './headers';

describe('Headers factory', () => {
  let factory: CreateHeaders;
  let createUtcDate: () => Date;
  let cryptoKi: CryptoKi;

  beforeEach(() => {
    const configuration = { accessKey: '12a' } as Configuration;
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    cryptoKi = new CryptoKiMock();
    const sdkVersion = 'TEST';
    factory = createHeadersFactory({
      configuration,
      createUtcDate,
      cryptoKi,
      sdkVersion
    });
  });

  beforeEach(() => {
    cryptoKi.sign = jest
      .fn()
      .mockImplementation(message => `${message}_signed`);
  });

  it('should add a custom header with timestamp', async () => {
    const headers = await factory('http://server/path');

    expect(headers['X-LGO-DATE']).toEqual(samples.june.getTime().toString());
  });

  it('should add an authorization header with a signature', async () => {
    const headers = await factory('http://server/path');
    const timestamp = samples.june.getTime();

    expect(headers.Authorization).toEqual(
      `LGO 12a:${timestamp}\nserver/path_signed`
    );
  });

  it('should add a custom header with sdkVersion', async () => {
    const headers = await factory('http://server/path');

    expect(headers['X-LGO-CLIENT-VERSION']).toEqual('TEST');
  });
});
