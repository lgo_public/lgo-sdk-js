import { createApiUserOrder, createUserOrder } from '../../../../test';
import { convertApiUserOrder, convertApiUserOrderPage } from './userOrders';

describe('User orders', () => {
  describe('while converting api user order', () => {
    it('should return data', () => {
      const apiOrder = createApiUserOrder({ id: '1' });

      const order = convertApiUserOrder(apiOrder);

      expect(order).toEqual(createUserOrder({ id: '1' }));
    });
  });

  describe('while converting api user order page', () => {
    it('should return data', () => {
      const apiPage = {
        result: {
          orders: [
            createApiUserOrder({ id: '1' }),
            createApiUserOrder({ id: '2' })
          ]
        },
        next_page: 'page42'
      };

      const page = convertApiUserOrderPage(apiPage);

      expect(page).toEqual({
        results: [createUserOrder({ id: '1' }), createUserOrder({ id: '2' })],
        nextPage: 'page42'
      });
    });
  });
});
