import { Key } from '../key';

export enum OrderSide {
  buy = 'B',
  sell = 'S'
}

export enum OrderType {
  market = 'M',
  limit = 'L'
}

export enum OrderStatus {
  sent = 'SENT',
  received = 'RECEIVED',
  pending = 'PENDING',
  open = 'OPEN',
  done = 'DONE',
  invalid = 'INVALID',
  ptsCheckFailure = 'PTS_CHECK_FAILURE'
}

export interface OrderToPlace {
  type: OrderType;
  side: OrderSide;
  productId: string;
  quantity: number;
  price?: number;
  key?: Key;
  reference?: number;
}

export interface OrderCancellation {
  reference?: number;
  key?: Key;
  orderId: string;
}

export interface OrderPrepared {
  keyId: string;
  order: string;
  signature: { source: string; value: string };
  reference?: number;
}

export interface CancellationPrepared {
  order_id: string;
  timestamp: number;
  signature: { source: string; value: string };
  reference?: number;
}

export interface UnEncryptedOrderPrepared {
  order: {
    type: OrderType;
    side: OrderSide;
    product_id: string;
    quantity: string;
    price?: string;
    timestamp: number;
  };
  signature: { source: string; value: string };
  reference?: number;
}
