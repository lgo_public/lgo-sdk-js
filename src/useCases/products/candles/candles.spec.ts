import { samples } from '../../../test';
import { ApiCandles, convertApiCandles } from './candles';

describe('Candles', () => {
  describe('while converting api candles', () => {
    it('should return structured ohlcv', () => {
      const apiCandles: ApiCandles = {
        prices: [
          [
            samples.mayIso8601,
            '7000.1',
            '8000.1',
            '7500.1',
            '7600.1',
            '10000.1'
          ],
          [
            samples.juneIso8601,
            '7003.1',
            '8003.1',
            '7503.1',
            '7603.1',
            '10003.1'
          ]
        ]
      };

      const candles = convertApiCandles(apiCandles);

      expect(candles).toEqual({
        prices: [
          {
            time: samples.may,
            low: 7000.1,
            high: 8000.1,
            open: 7500.1,
            close: 7600.1,
            volume: 10000.1
          },
          {
            time: samples.june,
            low: 7003.1,
            high: 8003.1,
            open: 7503.1,
            close: 7603.1,
            volume: 10003.1
          }
        ]
      });
    });
  });
});
