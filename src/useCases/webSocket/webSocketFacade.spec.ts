import {
  PrepareOrder,
  PrepareOrderCancellation,
  PrepareUnencryptedOrder,
  PrepareUnencryptedOrderCancellation
} from '../../domain';
import {
  createKey,
  createOrderPrepared,
  createOrderToPlace,
  WsMock
} from '../../test';
import { WebSocketClient } from '../../tools';
import { Feed, WebSocketFacade } from './webSocketFacade';

describe('Web socket facade', () => {
  let client: WebSocketClient;
  let prepareOrder: PrepareOrder;
  let prepareOrderCancellation: PrepareOrderCancellation;
  let prepareUnencryptedOrder: PrepareUnencryptedOrder;
  let prepareUnencryptedOrderCancellation: PrepareUnencryptedOrderCancellation;
  let facade: WebSocketFacade;

  beforeEach(() => {
    client = new WsMock();
    prepareOrder = jest.fn().mockResolvedValue(undefined);
    prepareOrderCancellation = jest.fn().mockResolvedValue(undefined);
    prepareUnencryptedOrder = jest.fn().mockResolvedValue(undefined);
    prepareUnencryptedOrderCancellation = jest.fn().mockResolvedValue(undefined);
    facade = new WebSocketFacade({
      client,
      prepareOrder,
      prepareOrderCancellation,
      prepareUnencryptedOrder,
      prepareUnencryptedOrderCancellation
    });
  });

  describe('on connect', () => {
    it('should delegate to client', async () => {
      await facade.connect();

      expect(client.connect).toHaveBeenCalled();
    });
  });

  describe('on disconnect', () => {
    it('should delegate to client', () => {
      facade.disconnect();

      expect(client.disconnect).toHaveBeenCalled();
    });
  });

  describe('on place order', () => {
    it('should prepare order first', async () => {
      (prepareOrder as any).mockResolvedValue(createOrderPrepared());
      const key = createKey({ id: '1337' });
      const order = createOrderToPlace();
      const orderPlacement = {
        ...order,
        key,
        reference: 42
      };

      await facade.placeOrder(orderPlacement);

      expect(prepareOrder).toHaveBeenCalledWith(orderPlacement);
    });

    it('should send preparation via client', async () => {
      const preparation = createOrderPrepared();
      (prepareOrder as any).mockResolvedValue(preparation);
      const key = createKey({ id: '1337' });
      const order = createOrderToPlace();

      await facade.placeOrder({
        ...order,
        key,
        reference: 42
      });

      const expectedMessage = JSON.stringify({
        type: 'placeorder',
        order: {
          key_id: preparation.keyId,
          order: preparation.order,
          reference: preparation.reference,
          signature: preparation.signature
        }
      });
      expect(client.send).toHaveBeenCalledWith(expectedMessage);
    });

    it('should reject if placement is malformed', async () => {
      const key = createKey({ id: '1337' });

      const call = facade.placeOrder({
        ...createOrderToPlace({ type: 3 } as any),
        key,
        reference: 42
      } as any);

      await expect(call).rejects.toThrow(
        'Expected property `type` to be of type `string` but received type `number`'
      );
    });
  });

  describe('on cancel order', () => {
    it('should prepare cancellation first', async () => {
      (prepareOrderCancellation as any).mockResolvedValue(
        createOrderPrepared()
      );
      const key = createKey({ id: '1337' });
      const orderCancellation = {
        orderId: '13',
        key,
        reference: 42
      };

      await facade.cancelOrder(orderCancellation);

      expect(prepareOrderCancellation).toHaveBeenCalledWith(orderCancellation);
    });

    it('should send preparation via client', async () => {
      const preparation = createOrderPrepared();
      (prepareOrderCancellation as any).mockResolvedValue(preparation);
      const key = createKey({ id: '1337' });

      await facade.cancelOrder({
        orderId: '13',
        key,
        reference: 42
      });

      const expectedMessage = JSON.stringify({
        type: 'placeorder',
        order: {
          key_id: preparation.keyId,
          order: preparation.order,
          reference: preparation.reference,
          signature: preparation.signature
        }
      });
      expect(client.send).toHaveBeenCalledWith(expectedMessage);
    });

    it('should reject if cancellation is malformed', async () => {
      const key = createKey({ id: '1337' });

      const call = facade.cancelOrder({
        key,
        reference: 42
      } as any);

      await expect(call).rejects.toThrow(
        'Expected property `orderId` to be of type `string` but received type `undefined`'
      );
    });
  });

  describe('on subscribe', () => {
    it('should send subscription via client', async () => {
      await facade.subscribe([
        { name: Feed.trades, productId: 'BTC-USD' },
        { name: Feed.afr }
      ]);

      const expectedMessage = JSON.stringify({
        type: 'subscribe',
        channels: [
          { name: Feed.trades, product_id: 'BTC-USD' },
          { name: Feed.afr }
        ]
      });
      expect(client.send).toHaveBeenCalledWith(expectedMessage);
    });

    it('should reject if subscription is malformed', async () => {
      const call = facade.subscribe([{ name: 3 } as any]);

      await expect(call).rejects.toThrow(
        'Expected property `name` to be of type `string` but received type `number`'
      );
    });
  });

  describe('on unsubscribe', () => {
    it('should send desubscription via client', async () => {
      await facade.unsubscribe([
        { name: Feed.trades, productId: 'BTC-USD' },
        { name: Feed.afr }
      ]);

      const expectedMessage = JSON.stringify({
        type: 'unsubscribe',
        channels: [
          { name: Feed.trades, product_id: 'BTC-USD' },
          { name: Feed.afr }
        ]
      });
      expect(client.send).toHaveBeenCalledWith(expectedMessage);
    });

    it('should reject if desubscription is malformed', async () => {
      const call = facade.unsubscribe([{ name: 3 } as any]);

      await expect(call).rejects.toThrow(
        'Expected property `name` to be of type `string` but received type `number`'
      );
    });
  });
});
