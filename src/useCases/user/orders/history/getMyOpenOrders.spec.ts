import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import {
  AxiosMock,
  createLiveApiUserOrder,
  createLiveUserOrder,
} from '../../../../test';
import { createGetMyOpenOrders, GetMyOpenOrders } from './getMyOpenOrders';

describe('Get my live open orders', () => {
  let axios: AxiosInstance;
  let getMyOpenOrders: GetMyOpenOrders;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getMyOpenOrders = createGetMyOpenOrders({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        batch_id: 2,
        orders: [
          createLiveApiUserOrder({ order_id: '1' }),
          createLiveApiUserOrder({ order_id: '2' })
        ]
      }
    });
  });

  it('should use provided query to get resource', async () => {
    await getMyOpenOrders({
      productId: 'BTC-USD',
    });

    const expectedUrl =
      'http://exchange/v1/live/orders?product_id=BTC-USD';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return last batch id', async () => {
    const result = await getMyOpenOrders({productId: 'BTC-USD'});

    expect(result.batchId).toEqual(2);
  });

  it('should return orders', async () => {
    const result = await getMyOpenOrders({productId: 'BTC-USD'});

    expect(result.orders).toEqual([createLiveUserOrder({ id: '1' }), createLiveUserOrder({ id: '2' })]);
  });

  it('should reject if product id is malformed', async () => {
    const call = getMyOpenOrders({ productId: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });
});
