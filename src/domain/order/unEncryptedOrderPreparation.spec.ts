import {
  createOrderToPlace,
  CryptoKiMock,
  samples
} from '../../test';
import { CreateUtcDate, CryptoKi } from '../../tools';
import {
  createPrepareUnEcryptedOrder,
  PrepareUnencryptedOrder,
  validateUnEncryptedOrderPlacement
} from './unEncryptedOrderPreparation';

describe('Unencrypted Order preparation', () => {
  let createUtcDate: CreateUtcDate;
  let prepareUnencryptedOrder: PrepareUnencryptedOrder;
  let cryptoKi: CryptoKi;

  beforeEach(() => {
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    cryptoKi = new CryptoKiMock();
    prepareUnencryptedOrder = createPrepareUnEcryptedOrder({
      createUtcDate,
      cryptoKi,
    });
  });

  it('should sign order', async () => {
    const order = createOrderToPlace();

    const result = await prepareUnencryptedOrder({
      ...order,
      reference: 42
    });

    const expectedPreparedOrder = {
      order: {
        price: '10',
        product_id: 'BTC-USD',
        type: 'L',
        side: 'S',
        quantity: '1',
        timestamp: 1527811200
      },
      signature: {
        source: 'RSA',
        value: '{"type":"L","side":"S","product_id":"BTC-USD","quantity":"1","price":"10","timestamp":1527811200}|signed'
      },
      reference: 42
    };
    expect(result).toEqual(expectedPreparedOrder);
  });

  describe('Unencrypted Order prepartion validation', () => {
    it("should fail if order's type is missing", async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace({ type: undefined } as any),
          reference: 42
        } as any);

      await expect(call).toThrow(
        'Expected property `type` to be of type `string` but received type `undefined`'
      );
    });

    it("should fail if order's side is missing", async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace({ side: undefined } as any),
          reference: 42
        } as any);

      await expect(call).toThrow(
        'Expected property `side` to be of type `string` but received type `undefined`'
      );
    });

    it("should fail if order's product id is missing", async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace({ productId: undefined } as any),
          reference: 42
        } as any);

      await expect(call).toThrow(
        'Expected property `productId` to be of type `string` but received type `undefined`'
      );
    });

    it("should fail if order's quantity is missing", async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace({ quantity: undefined } as any),
          reference: 42
        } as any);

      await expect(call).toThrow(
        'Expected property `quantity` to be of type `number` but received type `undefined`'
      );
    });

    it("should fail if order's price is malformed", async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace({ price: 'hello' } as any),
          reference: 42
        } as any);

      await expect(call).toThrow(
        'Expected property `price` to be of type `number` but received type `string`'
      );
    });

    it("won't fail if order's price is missing", async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace({ price: undefined } as any),
          reference: 42
        } as any);

      await expect(call).not.toThrow();
    });

    it('should fail if reference is malformed', async () => {
      const call = () =>
        validateUnEncryptedOrderPlacement({
          ...createOrderToPlace(),
          reference: 'hello'
        } as any);

      await expect(call).toThrow(
        'Expected property `reference` to be of type `number` but received type `string`'
      );
    });
  });
});
