import { convertApiCurrencies } from './currencies';

describe('Currencies', () => {
  describe('while converting api currencies', () => {
    it('should return currencies', async () => {
      const apiCurrencies = {
        currencies: [
          { name: 'Bitcoin', code: 'BTC', decimals: 8 },
          { name: 'United States Dollar', code: 'USD', decimals: 4 }
        ]
      };

      const currencies = convertApiCurrencies(apiCurrencies);

      expect(currencies).toEqual({
        currencies: [
          { name: 'Bitcoin', code: 'BTC', decimals: 8 },
          { name: 'United States Dollar', code: 'USD', decimals: 4 }
        ]
      });
    });
  });
});
