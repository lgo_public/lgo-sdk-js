import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import { AxiosMock } from '../../../test';
import { createGetOrderBookL2, GetOrderBookL2 } from './getOrderBookL2';

describe('Get order book level 2', () => {
  let axios: AxiosInstance;
  let getOrderBookL2: GetOrderBookL2;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getOrderBookL2 = createGetOrderBookL2({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        product_id: 'BTC-USD',
        bids: [['5.7', '1'], ['1.3', '5.8']],
        asks: [['6.1', '4.7'], ['6.3', '3.3']]
      }
    });
  });

  it('should use get corresponding resource', async () => {
    await getOrderBookL2({ productId: 'BTC-USD' });

    const expectedUrl = 'http://exchange/v1/live/products/BTC-USD/book?level=2';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return products', async () => {
    const result = await getOrderBookL2({ productId: 'BTC-USD' });

    expect(result).toEqual({
      productId: 'BTC-USD',
      bids: [{ price: 5.7, quantity: 1 }, { price: 1.3, quantity: 5.8 }],
      asks: [{ price: 6.1, quantity: 4.7 }, { price: 6.3, quantity: 3.3 }]
    });
  });

  it('should reject if productId is malformed', async () => {
    const call = getOrderBookL2({ productId: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });
});
