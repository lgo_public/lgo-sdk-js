import { RSA_PKCS1_OAEP_PADDING } from 'constants';
import { privateDecrypt } from 'crypto';

import { createKey } from '../../test';
import { encryptOrder, encryptOrderCancellation } from './orderEncryption';
import { OrderSide, OrderType } from './types';

const timestamp = Date.now();

const publicKey2048 = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvy48cijJAFzJ2uQ7k2o4
Sry+uD1ApLXSSH5dBvBHLN7Stk9fVcVdP6nIsnuE/HBUFCyIPQ1GYZI9GXK1HIZB
dJCJMsJRgOsUV9Wh2l5q/YeHYqdDpxXIPXyP1ZGVvb85C6QtuTrNQaAvS7aKS53a
Iqybez6Wqhw+3SDNDr9EFY0wbQlhdq3LPP+Z3XiwFUzaF8T1dpAQ/pVOD9pR0Bq2
bTfa7WXYFTAZsxKxbxXDpoeDRvMYk6WWEjdQbeqJrkMAvypLJAgux4PvSZX4TYG5
hA65olgk1OpODf6Jz+Ndb1Qj0BlZF2YJWDFWOlWcO/3rkBgcMKQFRCHN1MlczWaZ
ZQIDAQAB
-----END PUBLIC KEY-----`;

const privateKey2048 = `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAvy48cijJAFzJ2uQ7k2o4Sry+uD1ApLXSSH5dBvBHLN7Stk9f
VcVdP6nIsnuE/HBUFCyIPQ1GYZI9GXK1HIZBdJCJMsJRgOsUV9Wh2l5q/YeHYqdD
pxXIPXyP1ZGVvb85C6QtuTrNQaAvS7aKS53aIqybez6Wqhw+3SDNDr9EFY0wbQlh
dq3LPP+Z3XiwFUzaF8T1dpAQ/pVOD9pR0Bq2bTfa7WXYFTAZsxKxbxXDpoeDRvMY
k6WWEjdQbeqJrkMAvypLJAgux4PvSZX4TYG5hA65olgk1OpODf6Jz+Ndb1Qj0BlZ
F2YJWDFWOlWcO/3rkBgcMKQFRCHN1MlczWaZZQIDAQABAoIBAHBB4/hcfnoaAeTn
8xuCWX95CLwCK30LzmgG2vUCDakJRc0LGj2w4CpLta8n3FSmWJhn9a9zpgoQ5pOU
yUfwhWq+m/EULOt4Hb/dj2Y73rLz68k/0ffAUI3aIFMVY0nBiOuPAdIr9u478smQ
a/AuxjFqDPn3LFzCrR8G7aFlKcNE2jOdrH5L/pxyVaZhyEKRrf2C4MSUDoQ7lpG/
9JsNSBiWvCkcypYiKKtw1ETpmliF8i2sE0IqJFXUSRJL/T1hb6A2QFCRD7xyZ8pM
6a0QSrHH9k2fOXjliw7dQsB56GxRRVm5Ird3XCYym/hwAl2NPRmUMKAHWOYJX1Ga
yT/NkYECgYEA6IceArnkvrG7CMrxVw4gaR/auzYIMOOgakyq4rfaTdQ0A7vibQZb
OxMrceCgu4m6il1a9fRHTnzY6vTpyTEroasBcD+bc1BXDPDENaCvVOJIniApRaNG
LFvdQjxupWMTwO8L/Tnx/pilCZGS5RbsOTDpsc/zsdQ8S2YRP+TEhTUCgYEA0nqk
hUgw6huiHOpno0szuolmA8I8Jlp11e4CpbEBkzpAd1Ucadq2EDpgdUHBUFFF+dVD
PmAAPLNCwY1aN6AGiLLqLAv/asLaT+9VUdYRO917Yu6fEKJaitrvH2IyXH7E3ZVz
TFj2XcK1pmhCK9T2e96RPj5qXMtn7brt2IfjOXECgYADDDpVeNBJu5SjAd318SZ1
mkhIqeWjlmg03Hpk7Fn9KSnhXqYSHcI2XQL1xCb3Lk/2nlCh5qjEm5hQRBOIYo+a
19319a947HWnosQ/m7KQn0KqZ+uCkShLTozw4E/m9Qx+O+PhVtAYMTIAlRriy9Ee
0shNX1dnXkvIBTd2qe31fQKBgH1PeyfG5ShcL1Yb0HwNx8xlBRFz0K/c547oVzci
fReUdNEXIQV8qyxaNRdagKc62/h0GcbHDR0WdMYbS/0WDw3Px09WDUkYnTU9/oPs
ePI1WMJ74sPahSFqWzdKFUWwS7DIy45G4NbNl6xtIghKkD0LM1nlexmoGEYXJofl
z0hhAoGBALnlLdxMbVrtrLAresWF3/Tfo1MhExs8x3tFSHGRcx2JhDJFNaDiQugu
cMFakhuQMHU+THzsw2xpozyKjYrHs2Vy09eiiH2T6URqRpqI/oU2rZXjcP2pdNX0
vbUdOTdlQnV16DsHfwbFuYFZcTJI7rwg3gYO9P7JnTnbQCnqhlZo
-----END RSA PRIVATE KEY-----`;

describe('Order encryption', () => {
  describe('while encrypting an order', () => {
    it('should serialize it then encrypt it with provided public key', async () => {
      const order = {
        type: OrderType.limit,
        side: OrderSide.sell,
        productId: 'BTC-USD',
        quantity: 1,
        price: 10
      };
      const key = createKey({ publicKey: publicKey2048 });

      const encryption = await encryptOrder(key, order, timestamp);

      const decrypted = decrypt(encryption);
      expect(decrypted).toContain('BTC-USD');
      expect(decrypted).toContain(timestamp);
    });
  });

  describe('while encrypting a cancellation', () => {
    it('should serialize it then encrypt it with provided public key', async () => {
      const key = createKey({ publicKey: publicKey2048 });

      const encryption = await encryptOrderCancellation(key, '123', timestamp);

      const decrypted = decrypt(encryption);
      expect(decrypted).toContain('123');
      expect(decrypted).toContain(timestamp);
    });
  });

  function decrypt(base64Message: string): string {
    return privateDecrypt(
      {
        key: privateKey2048,
        padding: RSA_PKCS1_OAEP_PADDING
      },
      Buffer.from(base64Message, 'base64')
    ).toString();
  }
});
