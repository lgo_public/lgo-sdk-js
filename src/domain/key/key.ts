import ow from 'ow';

export interface KeySummary {
  id: string;
  enabledAt: Date;
  disabledAt: Date;
}

export interface Key extends KeySummary {
  publicKey: string;
}

export const keyShape = {
  id: ow.string,
  enabledAt: ow.date,
  disabledAt: ow.date,
  publicKey: ow.string
};
