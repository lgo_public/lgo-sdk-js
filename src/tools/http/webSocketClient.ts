import { EventEmitter } from 'events';
import Timeout = NodeJS.Timeout;
import { ClientRequest, IncomingMessage } from 'http';
import * as WebSocket from 'ws';

import { Logger } from '../logger';
import { CreateHeaders } from './headers';
import { wrapNodeResponseError } from './httpErrors';

export interface WebSocketClientCreation {
  url: string;
  createHeaders: CreateHeaders;
  connectionTimeoutDelay: number;
  reconnectionDelay: number;
  pingDelay: number;
  pongTimeoutDelay: number;
  logger: Logger;
}

export class WebSocketClient extends EventEmitter implements WebSocketClient {
  private instance: WebSocket | null = null;
  private readonly url: string;
  private readonly logger: Logger;
  private autoReconnect: boolean;
  private pingTimeout: Timeout | null = null;
  private pongTimeout: Timeout | null = null;
  private reconnectTimeout: Timeout | null = null;
  private connectionTimeoutDelay: number;
  private reconnectionDelay: number;
  private pingDelay: number;
  private pongTimeoutDelay: number;
  private readonly createHeaders: CreateHeaders;

  constructor(creation: WebSocketClientCreation) {
    super();
    const {
      url,
      logger,
      connectionTimeoutDelay,
      reconnectionDelay,
      pingDelay,
      pongTimeoutDelay,
      createHeaders
    } = creation;
    this.url = url;
    this.createHeaders = createHeaders;
    this.logger = logger;
    this.connectionTimeoutDelay = connectionTimeoutDelay;
    this.reconnectionDelay = reconnectionDelay;
    this.pingDelay = pingDelay;
    this.pongTimeoutDelay = pongTimeoutDelay;
    this.autoReconnect = false;
  }

  public async connect() {
    this.logger.log('Connecting');
    this.autoReconnect = true;
    const headers = await this.createHeaders(`${this.url}/`);
    this.instance = new WebSocket(this.url, {
      headers,
      handshakeTimeout: this.connectionTimeoutDelay
    });
    this.instance.on('open', this.onSocketOpen.bind(this));
    this.instance.on('message', this.onSocketMessage.bind(this));
    this.instance.on('close', this.onSocketClosed.bind(this));
    this.instance.on('error', this.onSocketError.bind(this));
    this.instance.on('ping', this.onSocketPing.bind(this));
    this.instance.on('pong', this.onSocketPong.bind(this));
    this.instance.on(
      'unexpected-response',
      this.onUnexpectedResponse.bind(this)
    );
  }

  private onSocketOpen() {
    this.logger.log('Socket opened');
    this.emit('open');
    this.startPinging();
  }

  private onSocketPing() {
    this.logger.log('Socket ping');
    this.emit('ping');
  }

  private onSocketPong() {
    this.logger.log('Socket pong');
    this.emit('pong');
    this.clearTimeouts(this.pongTimeout);
    this.startPinging();
  }

  private startPinging() {
    this.logger.log('Starting ping');
    this.clearTimeouts(this.pingTimeout);
    this.pingTimeout = setTimeout(() => {
      this.waitPong();
      this.ping();
    }, this.pingDelay);
  }

  private ping() {
    const instance = this.ensureOpened();
    instance.ping(null, undefined, (error?: Error) => {
      if (error) {
        this.logger.error(`Ping error: ${error.message}`);
      }
    });
  }

  private waitPong() {
    this.logger.log('Waiting for pong');
    this.pongTimeout = setTimeout(
      this.killConnection.bind(this),
      this.pongTimeoutDelay
    );
  }

  private killConnection() {
    this.logger.log('Pong not received in time, killing connection');
    if (this.instance) {
      this.emit('will-close');
      this.instance.close();
    }
  }

  private onSocketMessage(message: any) {
    this.logger.log('Socket message');
    this.emit('message', message);
  }

  private onSocketError(error: Error) {
    this.logger.error(`Socket error: ${error}`);
    this.emit('error', error);
  }

  private onUnexpectedResponse(_: ClientRequest, response: IncomingMessage) {
    this.logger.error(`Unexpected response (code: ${response.statusCode})`);
    this.emit('error', wrapNodeResponseError(response));
  }

  private onSocketClosed() {
    this.logger.log('Socket closed');
    this.emit('close');
    if (this.autoReconnect) {
      this.dispose();
      this.reconnect();
    }
  }

  private reconnect() {
    this.logger.log(`Reconnecting in ${this.reconnectionDelay}ms`);
    this.reconnectTimeout = setTimeout(() => {
      this.connect();
    }, this.reconnectionDelay);
  }

  public async send(payload: any) {
    const instance = this.ensureOpened();
    return new Promise((resolve, reject) => {
      try {
        instance.send(payload, (error?: Error) => {
          if (error) {
            reject(error);
          } else {
            resolve();
          }
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  public disconnect() {
    this.logger.log('Disconnecting');
    this.autoReconnect = false;
    this.dispose();
  }

  private dispose() {
    this.clearTimeouts(
      this.pingTimeout,
      this.pongTimeout,
      this.reconnectTimeout
    );
    if (this.instance !== null) {
      this.ensureClosed();
      this.instance.removeAllListeners();
    }
  }

  private ensureClosed() {
    if (
      this.instance !== null &&
      this.instance.readyState !== WebSocket.CONNECTING
    ) {
      this.instance.close();
    }
  }

  private ensureOpened(): WebSocket {
    if (this.instance === null || this.instance.readyState !== WebSocket.OPEN) {
      throw new Error('Web socket must be opened');
    }
    return this.instance;
  }

  private clearTimeouts(...timeouts: Array<Timeout | null>) {
    timeouts.forEach(t => {
      if (t) {
        clearTimeout(t);
      }
    });
  }
}
