export interface ApiCurrencies {
  currencies: ApiCurrency[];
}

export interface ApiCurrency {
  code: string;
  name: string;
  decimals: number;
}

export type Currencies = ApiCurrencies;

export type Currency = ApiCurrency;

export function convertApiCurrencies(apiCurrencies: ApiCurrencies): Currencies {
  return apiCurrencies;
}
