# Changelog

## 1.3.2 (unreleased)

todo

## 1.3.1 (2019-08-13)

### Changed

- Dependencies update and audit

## 1.3.0 (2019-08-06)

### Changed

- Environments are simplified to `production` and `sandbox`
- `markets-production` and `exchange-production` are merged to single `production` environment
- `markets-sandbox` and `exchange-sandbox` are merged to single `sandbox` environment

## 1.2.1 (2019-06-28)

### Changed

- Get my operations query is now optional

## 1.2.0 (2019-06-19)

### Added

- Sort direction can be provided for get orders or trades
- Orders can be filtered by status for get orders

### Fixed

- Reference is optional when placing or cancelling an order
- Timestamp precision is second when serializing an order before encryption

## 1.0.1 (2019-06-12)

### Added

- Get price history as candles

## 1.0.0 (2019-06-03)

Initial release
