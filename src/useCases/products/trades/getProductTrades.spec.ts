import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import {
  AxiosMock,
  createApiProductTrade,
  createProductTrade
} from '../../../test';
import { createGetProductTrades, GetProductTrades } from './getProductTrades';

describe('Get product trades', () => {
  let axios: AxiosInstance;
  let getProductTrades: GetProductTrades;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getProductTrades = createGetProductTrades({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        result: {
          trades: [
            createApiProductTrade({ id: '1' }),
            createApiProductTrade({ id: '2' })
          ]
        },
        next_page: 'page42'
      }
    });
  });

  it('should use provided query to get resource', async () => {
    await getProductTrades({ productId: 'BTC-USD', maxResults: 3, page: '42' });

    const expectedUrl =
      'http://exchange/v1/history/products/BTC-USD/trades?max_results=3&page=42';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return page', async () => {
    const result = await getProductTrades({ productId: 'BTC-USD' });

    expect(result).toEqual({
      results: [
        createProductTrade({ id: '1' }),
        createProductTrade({ id: '2' })
      ],
      nextPage: 'page42'
    });
  });

  it('should reject if product id is malformed', async () => {
    const call = getProductTrades({ productId: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });

  it('should reject if pagination is malformed', async () => {
    const call = getProductTrades({ maxResults: 'help me' } as any);

    await expect(call).rejects.toThrow(
      'Expected property `maxResults` to be of type `number` but received type `string`'
    );
  });
});
