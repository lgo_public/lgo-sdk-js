import ow from 'ow';

export interface ApiEntityPage<T> {
  result: {
    [key: string]: T[];
  };
  next_page: string;
}

export enum PaginationSort {
  ascending = 'ASC',
  descending = 'DESC'
}

export interface ApiPaginationQuery {
  max_results?: number;
  page?: string;
  sort?: PaginationSort;
}

export interface EntityPage<T> {
  results: T[];
  nextPage: string | null;
}

export interface PaginationQuery {
  maxResults?: number;
  page?: string;
  sort?: PaginationSort;
}

export const paginationShape = {
  maxResults: ow.optional.number,
  page: ow.optional.string,
  sort: ow.optional.string
};

export function convertPaginationQueryToApi(
  query: PaginationQuery
): ApiPaginationQuery {
  return {
    max_results: query.maxResults,
    page: query.page,
    sort: query.sort
  };
}

export type ApiEntityConverter<AT, DT> = (apiEntity: AT) => DT;

export type ApiEntitiesSelector<P extends ApiEntityPage<T>, T> = (
  page: P
) => T[];

export function convertApiPage<P extends ApiEntityPage<AT>, AT, DT>(
  page: P,
  apiEntitySelector: ApiEntitiesSelector<P, AT>,
  converter: ApiEntityConverter<AT, DT>
): EntityPage<DT> {
  return {
    results: apiEntitySelector(page).map(converter),
    nextPage: page.next_page || null
  };
}
