import { createKey, samples } from '../../test';
import { CreateUtcDate } from '../../tools';
import { Key } from './key';
import { createSelectKey, SelectKey } from './keySelector';

describe('Key selector', () => {
  let createUtcDate: CreateUtcDate;
  let selector: SelectKey;

  beforeEach(() => {
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    selector = createSelectKey({ createUtcDate });
  });

  it('should return a enabled now key', async () => {
    const keys = [
      createKey({ id: '1', enabledAt: samples.may, disabledAt: samples.june }),
      createKey({
        id: '2',
        enabledAt: samples.june,
        disabledAt: samples.july
      }),
      createKey({
        id: '3',
        enabledAt: samples.july,
        disabledAt: samples.august
      })
    ];

    const key = selector(keys);

    expect(key).toBeDefined();
    expect((key as any).id).toEqual('2');
  });

  it('should return the most available key if the most recent key is too close from period beginning', async () => {
    const keys = [
      createKey({
        id: '1',
        enabledAt: samples.midMay,
        disabledAt: samples.midJune
      }),
      createKey({
        id: '2',
        enabledAt: samples.june,
        disabledAt: samples.july
      })
    ];

    const key = selector(keys);

    expect(key).toBeDefined();
    expect((key as Key).id).toEqual('1');
  });

  it('should return the most available key if the less recent key is too close from period ending', async () => {
    const keys = [
      createKey({
        id: '1',
        enabledAt: samples.may,
        disabledAt: samples.juneTheThird
      }),
      createKey({
        id: '2',
        enabledAt: samples.midMay,
        disabledAt: samples.midJune
      })
    ];

    const key = selector(keys);

    expect(key).toBeDefined();
    expect((key as Key).id).toEqual('2');
  });

  it('should ignore a key still not enabled now', async () => {
    const keys = [
      createKey({
        id: '1',
        enabledAt: samples.june,
        disabledAt: samples.july
      }),
      createKey({
        id: '2',
        enabledAt: samples.july,
        disabledAt: samples.august
      })
    ];

    const key = selector(keys);

    expect(key).toBeDefined();
    expect((key as Key).id).toEqual('1');
  });

  it('should return nothing if all keys are disabled', () => {
    const keys = [
      createKey({
        id: '1',
        enabledAt: samples.march,
        disabledAt: samples.april
      }),
      createKey({
        id: '2',
        enabledAt: samples.april,
        disabledAt: samples.may
      })
    ];

    const key = selector(keys);

    expect(key).toBeUndefined();
  });

  it('should fail if keys are malformed', () => {
    const call = () => selector([{ id: 3 } as any]);

    expect(call).toThrow(
      'Expected property `id` to be of type `string` but received type `number`'
    );
  });
});
