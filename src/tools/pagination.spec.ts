import {
  ApiEntityPage,
  convertApiPage,
  convertPaginationQueryToApi,
  PaginationSort
} from './pagination';

interface ApiCat {
  the_name: string;
}

interface Cat {
  name: string;
}

interface ApiCatPage extends ApiEntityPage<ApiCat> {
  result: {
    cats: ApiCat[];
  };
}

function entitySelector(page: ApiCatPage) {
  return page.result.cats;
}

function convertApiCat(apiCat: ApiCat): Cat {
  return {
    name: apiCat.the_name
  };
}

describe('Pagination', () => {
  describe('on pagination query to api one', () => {
    it('should convert all properties', () => {
      const result = convertPaginationQueryToApi({
        maxResults: 12,
        page: 'A32',
        sort: PaginationSort.ascending
      });

      expect(result).toEqual({
        max_results: 12,
        page: 'A32',
        sort: PaginationSort.ascending
      });
    });

    it('could convert an empty object', () => {
      const result = convertPaginationQueryToApi({});

      expect(result).toEqual({});
    });
  });

  describe('on api page to domain one', () => {
    it('should convert', () => {
      const apiPage: ApiCatPage = {
        result: {
          cats: [{ the_name: 'Garfield' }, { the_name: 'Felix' }]
        },
        next_page: 'B36'
      };

      const page = convertApiPage(apiPage, entitySelector, convertApiCat);

      expect(page).toEqual({
        results: [{ name: 'Garfield' }, { name: 'Felix' }],
        nextPage: 'B36'
      });
    });

    it('should set next page as null if empty', () => {
      const apiPage: ApiCatPage = {
        result: {
          cats: []
        },
        next_page: ''
      };

      const page = convertApiPage(apiPage, entitySelector, convertApiCat);

      expect(page).toMatchObject({ nextPage: null });
    });
  });
});
