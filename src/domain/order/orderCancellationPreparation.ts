import ow from 'ow';

import { CreateUtcDate, CryptoKi } from '../../tools';
import { keyShape } from '../key';
import { EncryptOrderCancellation } from './orderEncryption';
import { OrderCancellation, OrderPrepared } from './types';

interface Dependencies {
  createUtcDate: CreateUtcDate;
  cryptoKi: CryptoKi;
  encryptOrderCancellation: EncryptOrderCancellation;
}

export type PrepareOrderCancellation = (
  cancellation: OrderCancellation
) => Promise<OrderPrepared | null>;

export function createPrepareOrderCancellation(
  dependencies: Dependencies
): PrepareOrderCancellation {
  const { createUtcDate, cryptoKi, encryptOrderCancellation } = dependencies;

  return async (cancellation: OrderCancellation) => {
    const { reference, key, orderId } = cancellation;
    if (!key) {
      return null;
    }
    const encryptedOrder = await encryptOrderCancellation(
      key,
      orderId,
      Math.floor(createUtcDate().getTime() / 1000)
    );
    const signedOrder = await cryptoKi.sign(encryptedOrder);
    const signature = {
      source: 'RSA',
      value: signedOrder
    };
    return {
      keyId: key.id,
      order: encryptedOrder,
      signature,
      reference
    };
  };
}

export function validateOrderCancellation(
  cancellation: OrderCancellation
): void {
  ow(
    cancellation,
    'cancellation',
    ow.object.exactShape({
      reference: ow.optional.number,
      key: keyShape,
      orderId: ow.string
    })
  );
}
