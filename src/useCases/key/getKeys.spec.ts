import { AxiosInstance } from 'axios';
import * as moment from 'moment';

import { Configuration } from '../../configuration';
import { AxiosMock, samples } from '../../test';
import { CreateUtcDate } from '../../tools';
import { createGetKeys, GetKeys } from './getKeys';

describe('Download keys', () => {
  let getKeys: GetKeys;
  let createUtcDate: CreateUtcDate;
  let axios: AxiosInstance;

  beforeEach(() => {
    const configuration = { keysUrl: 'key-server' } as Configuration;
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    axios = new AxiosMock();
    getKeys = createGetKeys({
      configuration,
      createUtcDate,
      axios
    });
  });

  it('should return downloaded keys', async () => {
    configureAxios();

    const keys = await getKeys();

    expect(keys).toEqual([
      {
        id: '123',
        enabledAt: moment.utc('2018-06-01T00:00:00Z').toDate(),
        disabledAt: moment.utc('2018-07-01T00:00:00Z').toDate(),
        publicKey: createPublicKey()
      },
      {
        id: '456',
        enabledAt: moment.utc('2018-07-01T00:00:00Z').toDate(),
        disabledAt: moment.utc('2018-08-01T00:00:00Z').toDate(),
        publicKey: createAnotherPublicKey()
      }
    ]);
  });

  it('should omit expired keys', async () => {
    (createUtcDate as any).mockReturnValue(samples.july);
    configureAxios();

    const keys = await getKeys();

    expect(keys).toHaveLength(1);
    expect(keys[0]).toMatchObject({ id: '456' });
  });

  it('should handle a poorly formatted index', async () => {
    configureAxios({ createIndex: createIndexPoorlyFormatted });

    const keys = await getKeys();

    expect(keys).toMatchObject([{ id: '123' }, { id: '456' }]);
  });

  function configureAxios(options = {}) {
    const safeOptions = {
      index: createIndex(),
      publicKey: createPublicKey(),
      anotherPUblicKey: createAnotherPublicKey(),
      ...options
    };
    const { index, publicKey, anotherPUblicKey } = safeOptions;
    axios.get = jest.fn().mockImplementation(url => {
      switch (url) {
        case 'key-server/index.txt':
          return { data: index };
        case 'key-server/123_public.pem':
          return { data: publicKey };
        case 'key-server/456_public.pem':
          return { data: anotherPUblicKey };
        default:
          throw new Error(`404: ${url}`);
      }
    });
  }

  function createIndex() {
    return (
      '123 2018-06-01T00:00:00Z 2018-07-01T00:00:00Z\n' +
      '456 2018-07-01T00:00:00Z 2018-08-01T00:00:00Z'
    );
  }

  function createIndexPoorlyFormatted() {
    return (
      '\n' +
      '123 2018-06-01T00:00:00Z 2018-07-01T00:00:00Z\n' +
      '456 2018-07-01T00:00:00Z 2018-08-01T00:00:00Z\n' +
      '\n'
    );
  }

  function createPublicKey() {
    return (
      '-----BEGIN PUBLIC KEY-----\n' +
      'MIIBIDANB\n' +
      '-----END PUBLIC KEY-----'
    );
  }

  function createAnotherPublicKey() {
    return (
      '-----BEGIN PUBLIC KEY-----\n' +
      'DANBMIIBI\n' +
      '-----END PUBLIC KEY-----'
    );
  }
});
