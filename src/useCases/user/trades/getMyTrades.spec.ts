import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import { AxiosMock, createApiUserTrade, createUserTrade } from '../../../test';
import { createGetMyTrades, GetMyTrades } from './getMyTrades';

describe('Get my trades', () => {
  let axios: AxiosInstance;
  let getMyTrades: GetMyTrades;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getMyTrades = createGetMyTrades({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        result: {
          trades: [
            createApiUserTrade({ id: '1' }),
            createApiUserTrade({ id: '2' })
          ]
        },
        next_page: 'page42'
      }
    });
  });

  it('should get corresponding resource', async () => {
    await getMyTrades();

    const expectedUrl = 'http://exchange/v1/history/trades';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should use provided query to get resource', async () => {
    await getMyTrades({ productId: 'BTC-USD', maxResults: 3, page: '42' });

    const expectedUrl =
      'http://exchange/v1/history/trades?max_results=3&page=42&product_id=BTC-USD';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return page', async () => {
    const result = await getMyTrades();

    expect(result).toEqual({
      results: [createUserTrade({ id: '1' }), createUserTrade({ id: '2' })],
      nextPage: 'page42'
    });
  });

  it('should reject if product id is malformed', async () => {
    const call = getMyTrades({ productId: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });

  it('should reject if pagination is malformed', async () => {
    const call = getMyTrades({ maxResults: 'help me' } as any);

    await expect(call).rejects.toThrow(
      'Expected property `maxResults` to be of type `number` but received type `string`'
    );
  });
});
