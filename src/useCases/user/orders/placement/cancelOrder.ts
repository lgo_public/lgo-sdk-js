import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import {
  OrderCancellation,
  PrepareOrderCancellation,
  validateOrderCancellation
} from '../../../../domain';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
  prepareOrderCancellation: PrepareOrderCancellation;
}

export type CancelOrder = (cancellation: OrderCancellation) => Promise<void>;

export function createCancelOrder(dependencies: Dependencies): CancelOrder {
  const { axios, configuration, prepareOrderCancellation } = dependencies;

  return async (cancellation: OrderCancellation) => {
    if (cancellation.key) {
      validateOrderCancellation(cancellation);
      const preparation = await prepareOrderCancellation(cancellation);
      if (preparation) {
        const body = {
          key_id: preparation.keyId,
          order: preparation.order,
          signature: preparation.signature,
          reference: preparation.reference
        };
        await axios.post(
          `${configuration.exchangeUrl}/v1/live/orders/encrypted`,
          body
        );
      }
    } else {
      await axios.delete(
        `${configuration.exchangeUrl}/v1/live/orders/${cancellation.orderId}`
      );
    }
  };
}
