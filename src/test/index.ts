export * from './axios';
export * from './emitter';
export * from './cryptoKi';
export * from './mock';
export * from './samples';
export * from './ws';

export function waitALittle() {
  return wait(50);
}

export function wait(delay: number) {
  return new Promise(resolve => setTimeout(resolve, delay));
}
