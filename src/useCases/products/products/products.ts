export interface ApiProducts {
  products: ApiProduct[];
}

export interface ApiProduct {
  id: string;
  base: ApiProductBase;
  quote: ApiProductQuote;
  total: ApiProductTotal;
}

export interface ApiProductBase {
  id: string;
  limits: { min: string; max: string };
}

export interface ApiProductQuote {
  id: string;
  limits: { min: string; max: string };
  increment: string;
}

export interface ApiProductTotal {
  limits: { min: string; max: string };
}

export interface Products {
  products: Product[];
}

export interface Product {
  id: string;
  base: ProductBase;
  quote: ProductQuote;
  total: ProductTotal;
}

export interface ProductBase {
  id: string;
  limits: { min: number; max: number };
}

export interface ProductQuote {
  id: string;
  limits: { min: number; max: number };
  increment: number;
}

export interface ProductTotal {
  limits: { min: number; max: number };
}

export function convertApiProducts(apiProducts: ApiProducts): Products {
  return {
    products: apiProducts.products.map(p => ({
      id: p.id,
      base: convertApiBase(p.base),
      quote: convertApiQuote(p.quote),
      total: convertApiTotal(p.total)
    }))
  };
}

function convertApiBase(apiBase: ApiProductBase): ProductBase {
  return {
    id: apiBase.id,
    limits: {
      min: Number.parseFloat(apiBase.limits.min),
      max: Number.parseFloat(apiBase.limits.max)
    }
  };
}

function convertApiQuote(apiQuote: ApiProductQuote): ProductQuote {
  return {
    id: apiQuote.id,
    increment: Number.parseFloat(apiQuote.increment),
    limits: {
      min: Number.parseFloat(apiQuote.limits.min),
      max: Number.parseFloat(apiQuote.limits.max)
    }
  };
}

function convertApiTotal(apiTotal: ApiProductTotal): ProductTotal {
  return {
    limits: {
      min: Number.parseFloat(apiTotal.limits.min),
      max: Number.parseFloat(apiTotal.limits.max)
    }
  };
}
