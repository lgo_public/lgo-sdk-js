import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../../configuration';
import { OrderStatus } from '../../../../domain';
import {
  convertPaginationQueryToApi,
  createUrl,
  PaginationQuery,
  paginationShape
} from '../../../../tools';
import {
  ApiUserOrderPage,
  convertApiUserOrderPage,
  UserOrderPage
} from './userOrders';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetMyOrdersQuery extends PaginationQuery {
  productId?: string;
  status?: OrderStatus;
}

export type GetMyOrders = (query?: GetMyOrdersQuery) => Promise<UserOrderPage>;

export function createGetMyOrders(dependencies: Dependencies): GetMyOrders {
  const { axios, configuration } = dependencies;

  return async (query: GetMyOrdersQuery = {}) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        ...paginationShape,
        productId: ow.optional.string,
        status: ow.optional.string
      })
    );
    const apiQuery = Object.assign(convertPaginationQueryToApi(query), {
      product_id: query.productId,
      status: query.status
    });
    const { data } = await axios.get<ApiUserOrderPage>(
      createUrl(`${configuration.exchangeUrl}/v1/history/orders`, apiQuery)
    );
    return convertApiUserOrderPage(data);
  };
}
