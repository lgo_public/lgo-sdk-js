import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../../configuration';
import { ApiUserOrder, convertApiUserOrder, UserOrder } from './userOrders';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export type GetMyOrder = (query: GetMyOrderQuery) => Promise<UserOrder>;

export interface GetMyOrderQuery {
  id: string;
}

export function createGetMyOrder(dependencies: Dependencies): GetMyOrder {
  const { axios, configuration } = dependencies;

  return async (query: GetMyOrderQuery) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        id: ow.optional.string
      })
    );
    const { data } = await axios.get<ApiUserOrder>(
      `${configuration.exchangeUrl}/v1/history/orders/${query.id}`
    );
    return convertApiUserOrder(data);
  };
}
