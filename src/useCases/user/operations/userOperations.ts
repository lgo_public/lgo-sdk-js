import * as moment from 'moment';

import { ApiEntityPage, convertApiPage, EntityPage } from '../../../tools';

export interface ApiUserOperation {
  id: string;
  account_id: string;
  owner_id: number;
  type: OperationType;
  status: OperationStatus;
  quantity: string;
  currency: string;
  counter_party: string;
  created_at: number;
  protocol: AddressProtocol;
}

export interface ApiUserOperationPage extends ApiEntityPage<ApiUserOperation> {
  result: {
    operations: ApiUserOperation[];
  };
}

export interface UserOperation {
  id: string;
  accountId: string;
  ownerId: number;
  type: OperationType;
  status: OperationStatus;
  quantity: number;
  currency: string;
  counterParty: string;
  createdAt: Date;
  protocol: AddressProtocol;
}

export enum AddressProtocol {
  onChain = 'ONCHAIN',
  pax = 'PAX',
  usdc = 'USDC',
  signet = 'SIGNET'
}

export enum OperationType {
  deposit = 'DEPOSIT',
  withdrawal = 'WITHDRAWAL'
}

export enum OperationStatus {
  waitingAmlApproval = 'WAITING_AML_APPROVAL',
  reconciled = 'RECONCILED',
  rejected = 'REJECTED',
  amlApproved = 'AML_APPROVED',
  canceled = 'CANCELED',
  accepted = 'ACCEPTED',
  offsetting = 'OFFSETTING',
  offsettingReconciled = 'OFFSETTING_RECONCILED'
}

export type UserOperationPage = EntityPage<UserOperation>;

export function convertApiUserOperationPage(
  apiUserOperationPage: ApiUserOperationPage
): UserOperationPage {
  return convertApiPage(
    apiUserOperationPage,
    p => p.result.operations,
    convertApiUserOperation
  );
}

function convertApiUserOperation(
  apiUserOperation: ApiUserOperation
): UserOperation {
  return {
    id: apiUserOperation.id,
    accountId: apiUserOperation.account_id,
    ownerId: apiUserOperation.owner_id,
    type: apiUserOperation.type,
    status: apiUserOperation.status,
    quantity: Number.parseFloat(apiUserOperation.quantity),
    currency: apiUserOperation.currency,
    counterParty: apiUserOperation.counter_party,
    createdAt: moment(apiUserOperation.created_at).toDate(),
    protocol: apiUserOperation.protocol
  };
}
