import * as moment from 'moment';

import {
  Key,
  OrderPrepared,
  OrderSide,
  OrderStatus,
  OrderToPlace,
  OrderType
} from '../domain';
import {
  AddressProtocol,
  ApiProductTrade,
  ApiUserOperation,
  ApiUserOrder,
  ApiUserTrade, LiveApiUserOrder,
  OperationStatus,
  OperationType,
  ProductTrade,
  UserOperation,
  UserOrder,
  UserTrade
} from '../useCases';

export const samples = {
  march: moment.utc('2018-03-01').toDate(),
  marchMsTimestamp: moment.utc('2018-03-01').valueOf(),
  april: moment.utc('2018-04-01').toDate(),
  aprilIso8601: moment.utc('2018-04-01').toISOString(),
  may: moment.utc('2018-05-01').toDate(),
  mayIso8601: moment.utc('2018-05-01').toISOString(),
  midMay: moment.utc('2018-05-15').toDate(),
  june: moment.utc('2018-06-01').toDate(),
  juneIso8601: moment.utc('2018-06-01').toISOString(),
  juneTheThird: moment.utc('2018-06-03').toDate(),
  midJune: moment.utc('2018-06-15').toDate(),
  july: moment.utc('2018-07-01').toDate(),
  august: moment.utc('2018-08-01').toDate()
};

export function createApiProductTrade(
  data: Partial<ApiProductTrade> = {}
): ApiProductTrade {
  return Object.assign(
    {
      id: '123',
      product_id: 'BTC-USD',
      quantity: '10',
      price: '5',
      buyer_fee: '0.1',
      seller_fee: '3.2',
      creation_date: moment.utc('2018-07-18T13:30:04.416Z').toDate(),
      side: OrderSide.buy,
      order_buyer_id: '28000',
      order_seller_id: '300'
    },
    data
  );
}

export function createProductTrade(data: Partial<ProductTrade>): ProductTrade {
  return Object.assign(
    {
      id: '123',
      productId: 'BTC-USD',
      quantity: 10,
      price: 5,
      buyerFee: 0.1,
      sellerFee: 3.2,
      creationDate: moment.utc('2018-07-18T13:30:04.416Z').toDate(),
      side: OrderSide.buy,
      orderBuyerId: '28000',
      orderSellerId: '300'
    },
    data
  );
}

export function createApiUserTrade(
  data: Partial<ApiUserTrade> = {}
): ApiUserTrade {
  return Object.assign(
    {
      id: '123',
      product_id: 'BTC-USD',
      quantity: '10',
      price: '5',
      fees: '0.1',
      creation_date: moment.utc('2018-07-18T13:30:04.416Z').toDate(),
      side: OrderSide.buy,
      order_id: '28000'
    },
    data
  );
}

export function createUserTrade(data: Partial<UserTrade>): UserTrade {
  return Object.assign(
    {
      id: '123',
      productId: 'BTC-USD',
      quantity: 10,
      price: 5,
      fees: 0.1,
      creationDate: moment.utc('2018-07-18T13:30:04.416Z').toDate(),
      side: OrderSide.buy,
      orderId: '28000'
    },
    data
  );
}

export function createApiUserOrder(
  data: Partial<ApiUserOrder> = {}
): ApiUserOrder {
  return Object.assign(
    {
      id: '123',
      batch_id: '2000',
      type: OrderType.limit,
      side: OrderSide.buy,
      product_id: 'BTC-USD',
      quantity: '100000000',
      remaining_quantity: '0',
      status: OrderStatus.done,
      price: '100000',
      creation_date: '2018-07-17T13:35:04.416Z'
    },
    data
  );
}

export function createLiveApiUserOrder(
  data: Partial<LiveApiUserOrder> = {}
): LiveApiUserOrder {
  return Object.assign(
    {
      order_id: '123',
      order_type: OrderType.limit,
      side: OrderSide.buy,
      product_id: 'BTC-USD',
      quantity: '100000000',
      remaining_quantity: '0',
      status: OrderStatus.done,
      price: '100000',
      order_creation_time: '2018-07-17T13:35:04.416Z'
    },
    data
  );
}

export function createUserOrder(data: Partial<UserOrder>): UserOrder {
  return Object.assign(
    {
      id: '123',
      type: OrderType.limit,
      side: OrderSide.buy,
      productId: 'BTC-USD',
      quantity: 100000000,
      remainingQuantity: 0,
      status: OrderStatus.done,
      price: 100000,
      creationDate: moment.utc('2018-07-17T13:35:04.416Z').toDate()
    },
    data
  );
}

export function createLiveUserOrder(data: Partial<UserOrder>): UserOrder {
  return Object.assign(
    {
      id: '123',
      type: OrderType.limit,
      side: OrderSide.buy,
      productId: 'BTC-USD',
      quantity: 100000000,
      remainingQuantity: 0,
      status: OrderStatus.open,
      price: 100000,
      creationDate: moment.utc('2018-07-17T13:35:04.416Z').toDate()
    },
    data
  );
}


export function createOrderToPlace(
  data: Partial<OrderToPlace> = {}
): OrderToPlace {
  return Object.assign(
    {
      type: OrderType.limit,
      side: OrderSide.sell,
      productId: 'BTC-USD',
      quantity: 1,
      price: 10
    },
    data
  );
}

export function createOrderPrepared(
  data: Partial<OrderPrepared> = {}
): OrderPrepared {
  return Object.assign(
    {
      keyId: '1337',
      order: 'encrypted',
      signature: { source: 'RSA', value: 'signed' },
      reference: 42
    },
    data
  );
}

export function createKey(data: Partial<Key> = {}): Key {
  return Object.assign(
    {
      id: '1',
      enabledAt: samples.may,
      disabledAt: samples.june,
      publicKey: 'pubkey'
    },
    data
  );
}

export function createApiUserOperation(
  data: Partial<ApiUserOperation>
): ApiUserOperation {
  return Object.assign(
    {
      id: '4d74e412-44ae-41df-9c65-fd81ce53fd78',
      account_id: 'cd49ed6f-1649-4418-857e-78be55bb432a',
      owner_id: 471671,
      type: OperationType.deposit,
      status: OperationStatus.amlApproved,
      quantity: '1.00000000',
      currency: 'BTC',
      counter_party: 'bc1qjl8uwezzlech723lpnyuza0h2cdkvxvh54v3dn',
      created_at: samples.marchMsTimestamp,
      protocol: AddressProtocol.onChain
    },
    data
  );
}

export function createUserOperation(
  data: Partial<UserOperation>
): UserOperation {
  return Object.assign(
    {
      id: '4d74e412-44ae-41df-9c65-fd81ce53fd78',
      accountId: 'cd49ed6f-1649-4418-857e-78be55bb432a',
      ownerId: 471671,
      type: OperationType.deposit,
      status: OperationStatus.amlApproved,
      quantity: 1,
      currency: 'BTC',
      counterParty: 'bc1qjl8uwezzlech723lpnyuza0h2cdkvxvh54v3dn',
      createdAt: samples.march,
      protocol: AddressProtocol.onChain
    },
    data
  );
}
