import { AxiosInstance } from 'axios';

import { Configuration } from '../../../configuration';
import { ApiProducts, convertApiProducts, Products } from './products';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export type GetProducts = () => Promise<Products>;

export function createGetProducts(dependencies: Dependencies): GetProducts {
  const { axios, configuration } = dependencies;

  return async () => {
    const { data } = await axios.get<ApiProducts>(
      `${configuration.exchangeUrl}/v1/live/products`
    );
    return convertApiProducts(data);
  };
}
