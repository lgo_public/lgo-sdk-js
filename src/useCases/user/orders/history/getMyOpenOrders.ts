import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../../configuration';
import {
  createUrl
} from '../../../../tools';
import {
  convertLiveApiUserOrders,
  LiveApiUserOrders,
  LiveOrders,
} from './userOrders';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetMyOpenOrdersQuery {
  productId: string;
}

export type GetMyOpenOrders = (query: GetMyOpenOrdersQuery) => Promise<LiveOrders>;

export function createGetMyOpenOrders(dependencies: Dependencies): GetMyOpenOrders {
  const { axios, configuration } = dependencies;

  return async (query: GetMyOpenOrdersQuery) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        productId: ow.string
      })
    );
    const apiQuery = {product_id: query.productId};
    const { data } = await axios.get<LiveApiUserOrders>(
      createUrl(`${configuration.exchangeUrl}/v1/live/orders`, apiQuery)
    );
    return convertLiveApiUserOrders(data, query.productId);
  };
}
