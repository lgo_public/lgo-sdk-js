import { createApiProductTrade, createProductTrade } from '../../../test';
import { convertApiProductTradePage } from './productTrades';

describe('Product trades', () => {
  describe('while converting api product trade page', () => {
    it('should return data', () => {
      const apiPage = {
        result: {
          trades: [
            createApiProductTrade({ id: '1' }),
            createApiProductTrade({ id: '2' })
          ]
        },
        next_page: 'page42'
      };

      const page = convertApiProductTradePage(apiPage);

      expect(page).toEqual({
        results: [
          createProductTrade({ id: '1' }),
          createProductTrade({ id: '2' })
        ],
        nextPage: 'page42'
      });
    });
  });
});
