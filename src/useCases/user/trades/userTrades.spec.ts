import { createApiUserTrade, createUserTrade } from '../../../test';
import { convertApiUserTradePage } from './userTrades';

describe('User trades', () => {
  describe('while converting api user trade page', () => {
    it('should return data', () => {
      const apiPage = {
        result: {
          trades: [
            createApiUserTrade({ id: '1' }),
            createApiUserTrade({ id: '2' })
          ]
        },
        next_page: 'page42'
      };

      const page = convertApiUserTradePage(apiPage);

      expect(page).toEqual({
        results: [createUserTrade({ id: '1' }), createUserTrade({ id: '2' })],
        nextPage: 'page42'
      });
    });
  });
});
