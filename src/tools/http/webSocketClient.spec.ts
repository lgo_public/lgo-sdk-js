import { orderBy } from 'lodash/fp';

import { wait, waitALittle } from '../../test';
import { ApiError, ErrorCode } from '../apiError';
import { createSilentLogger } from '../logger';
import { WsTestClient, WsTestServer } from './test';

describe('Web socket client', () => {
  let port = 2000;
  let server: WsTestServer;
  let client: WsTestClient;

  beforeEach(() => {
    port++;
    client = new WsTestClient({
      url: `ws://localhost:${port}`,
      connectionTimeoutDelay: 100,
      reconnectionDelay: 100,
      pingDelay: 100,
      pongTimeoutDelay: 500,
      logger: createSilentLogger(),
      createHeaders: url => Promise.resolve({ headers: `for ${url}` })
    });
    server = new WsTestServer(port);
  });

  describe('before connection', () => {
    it('should reject when message must be sent', async () => {
      const act = client.instance.send('hello world');

      await expect(act).rejects.toThrow('Web socket must be opened');
    });
  });

  describe('when client verification fails', () => {
    beforeEach(async () => {
      await server.start();
    });

    afterEach(async () => {
      await server.stop();
    });

    it('should notify error', async () => {
      await server.rejectClients();

      client.instance.connect();

      await wait(300);
      const errors = client.errorsReceived;
      expect(errors.length).toEqual(1);
      expect(errors[0]).toBeInstanceOf(ApiError);
      expect((errors[0] as ApiError).data.code).toEqual(ErrorCode.unauthorized);
    });

    it("won't do anything on disconnect", async () => {
      await server.rejectClients();
      client.instance.connect();
      await wait(300);
      const errorsReceived = client.errorsReceived.length;

      client.instance.disconnect();

      expect(client.errorsReceived).toHaveLength(errorsReceived);
    });
  });

  describe('when connection takes too much time', () => {
    beforeEach(async () => {
      await server.start();
    });

    afterEach(async () => {
      await server.stop();
    });

    it('should notify error', async () => {
      await server.timeoutConnections();

      client.instance.connect();

      await wait(300);
      const errors = client.errorsReceived;
      expect(errors.map(e => e.message)).toContain(
        'Opening handshake has timed out'
      );
    });
  });

  describe('after connection', () => {
    beforeEach(async done => {
      await server.start();
      client.instance.on('open', done);
      client.instance.connect();
    });

    afterEach(async () => {
      client.instance.disconnect();
      await server.stop();
    });

    it('should have sent headers based on url', () => {
      expect(server.clientHeaders).toMatchObject({
        headers: `for ws://localhost:${port}/`
      });
    });

    it('should notify open', async () => {
      expect(client.openReceivedCount).toEqual(1);
    });

    it('should notify received messages', async () => {
      await server.connectedSocket!.send('hello world');

      await waitALittle();
      expect(client.messagesReceived).toEqual(['hello world']);
    });

    it('should send message', async () => {
      const messages: string[] = [];
      server.connectedSocket!.on('message', m => messages.push(m.toString()));

      await client.instance.send('hello world');

      await waitALittle();
      expect(messages).toEqual(['hello world']);
    });

    it('should reject if cannot send message', async () => {
      server.closeClient();
      await waitALittle();

      const act = client.instance.send('hello world');

      await expect(act).rejects.toThrow('Web socket must be opened');
    });

    it('should try to reconnect if socket is closed', async () => {
      server.closeClient();

      await wait(300);
      expect(server.connectionReceivedCount).toBeGreaterThan(1);
    });

    it("won't try to reconnect after a manual disconnection", async () => {
      client.instance.disconnect();

      server.closeClient();

      await wait(300);
      expect(server.connectionReceivedCount).toEqual(1);
    });

    it("won't try to reconnect after a manual disconnection though a reconnection may be in progress", async () => {
      server.closeClient();
      await wait(300);
      server.connectionReceivedCount = 1;

      client.instance.disconnect();

      await wait(300);
      expect(server.connectionReceivedCount).toEqual(1);
    });

    it('should ping server periodically', async () => {
      await wait(1000);

      expect(server.pingReceivedTimes.length).toBeGreaterThan(0);
    });

    it('should ping only after a pong', async () => {
      await wait(1000);

      const allPings = server.pingReceivedTimes.map(t => ({
        type: 'ping',
        time: t
      }));
      const allPongs = client.pongReceivedTimes.map(t => ({
        type: 'pong',
        time: t
      }));
      const allPingAndPongs = allPings.concat(allPongs);
      const orderedPingAndPongs = orderBy(['time'], ['asc'], allPingAndPongs);
      for (let i = 0; i < orderedPingAndPongs.length; i++) {
        const { type } = orderedPingAndPongs[i];
        if (i % 2 === 0) {
          expect(type).toEqual('ping');
        } else {
          expect(type).toEqual('pong');
        }
      }
    });

    it('should close connection if a pong is not received in time', async () => {
      server.neverPong();

      await wait(1000);

      expect(client.willCloseReceivedCount).toBeGreaterThan(0);
      expect(client.closedReceivedCount).toBeGreaterThan(0);
    });

    it("won't close connection if a pong is received in time", async () => {
      await waitALittle();

      expect(client.closedReceivedCount).toEqual(0);
    });
  });
});
