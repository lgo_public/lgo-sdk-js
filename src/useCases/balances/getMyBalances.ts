
import { AxiosInstance } from 'axios';

import { Configuration } from '../../configuration';
import { ApiBalances, BalancesByCurrencyCode, convertApiBalances } from './balances';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export type GetMyBalances = () => Promise<BalancesByCurrencyCode>;

export function createGetMyBalances(dependencies: Dependencies): GetMyBalances {
  const { axios, configuration } = dependencies;

  return async () => {
    const { data } = await axios.get<ApiBalances>(
      `${configuration.exchangeUrl}/v1/live/balances`
    );
    return convertApiBalances(data);
  };
}
