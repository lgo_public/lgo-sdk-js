import { ApiError } from './apiError';

describe('Api error', () => {
  it('could be created with message and code', () => {
    const error = new ApiError('Something failed', { code: 'BLEH' });

    expect(error.message).toContain('Something failed (code: BLEH)');
    expect(error.data).toEqual({ code: 'BLEH' });
  });
});
