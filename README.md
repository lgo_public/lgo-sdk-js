# @lgo/sdk

LGO API SDK for JavaScript or TypeScript clients

## Requirements

LGO sdk requires the following to run:

- [Node.js](https://nodejs.org) 8 or 10

## Installation

```
npm install @lgo/sdk --save
```

## Examples

Get my orders:

```typescript
import { Client } from '@lgo/sdk';
// or: const { Client } = require('@lgo/sdk');

const client = new Client(/* options */);

client.getMyOrders().then(console.log);
```

Subscribe to trades channel:

```javascript
import { Client } from '@lgo/sdk';

const client = new Client(/* options */);

const ws = client.webSocket;

ws.on('error', console.error);
ws.on('message', console.log);

ws.on('open', () => {
  ws.subscribe([{ name: Feed.trades, productId: 'BTC-USD' }]);
});

ws.connect();
```

## Features

Via HTTP endpoints:

- Get available currencies
- Get available products
- Get order book level 1
- Get order book level 2
- Get product trades
- Get price history as candles
- Get my orders
- Get one of my orders
- Place an order
- Cancel an order
- Get my trades
- Get accounting operations

Via WebSockets:

- Subscribe to afr channel (orders in transit)
- Subscribe to user channel (orders status update)
- Subscribe to order book level 2 channel
- Subscribe to my balance channel
- Subscribe to my trades channel
- Place an order
- Cancel an order

## Environments

LGO sdk can be configured to target production or sandbox environments.

See [Environments](https://doc.exchange.lgo.markets/#_environments).

## Docs

- [API](https://doc.exchange.lgo.markets/)

## License

[MIT](LICENSE)
