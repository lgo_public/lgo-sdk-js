import { EventEmitter } from 'events';
import { isUndefined, omitBy } from 'lodash/fp';
import ow from 'ow';

import {
  CancellationPrepared,
  OrderCancellation,
  OrderPrepared,
  OrderToPlace,
  PrepareOrder,
  PrepareOrderCancellation,
  PrepareUnencryptedOrder,
  PrepareUnencryptedOrderCancellation,
  UnEncryptedOrderPrepared,
  validateOrderCancellation,
  validateOrderPlacement,
  validateUnencryptedOrderCancellation,
  validateUnEncryptedOrderPlacement
} from '../../domain';
import { WebSocketClient } from '../../tools';

export enum Feed {
  afr = 'afr',
  balance = 'balance',
  level2 = 'level2',
  trades = 'trades',
  user = 'user'
}

export type FeedChannel =
  | { name: Feed.afr }
  | { name: Feed.balance }
  | { name: Feed.level2; productId: string }
  | { name: Feed.trades; productId: string }
  | { name: Feed.user; productId: string };

const feedChannelShape = { name: ow.string, productId: ow.optional.string };

interface Creation {
  client: WebSocketClient;
  prepareOrder: PrepareOrder;
  prepareOrderCancellation: PrepareOrderCancellation;
  prepareUnencryptedOrder: PrepareUnencryptedOrder;
  prepareUnencryptedOrderCancellation: PrepareUnencryptedOrderCancellation;
}

export class WebSocketFacade extends EventEmitter {
  private readonly client: WebSocketClient;
  private readonly prepareOrder: PrepareOrder;
  private readonly prepareOrderCancellation: PrepareOrderCancellation;
  private readonly prepareUnEncryptedOrder: PrepareUnencryptedOrder;
  private readonly prepareUnencryptedOrderCancellation: PrepareUnencryptedOrderCancellation;

  constructor(creation: Creation) {
    super();
    const {
      client,
      prepareOrder,
      prepareOrderCancellation,
      prepareUnencryptedOrder,
      prepareUnencryptedOrderCancellation
    } = creation;
    this.client = client;
    this.prepareOrder = prepareOrder;
    this.prepareUnEncryptedOrder = prepareUnencryptedOrder;
    this.prepareOrderCancellation = prepareOrderCancellation;
    this.prepareUnencryptedOrderCancellation = prepareUnencryptedOrderCancellation;
    this.client.on('error', error => this.emit('error', error));
    this.client.on('message', message =>
      this.emit('message', JSON.parse(message))
    );
    this.client.on('open', () => this.emit('open'));
    this.client.on('close', () => this.emit('close'));
  }

  public async connect() {
    await this.client.connect();
  }

  public disconnect() {
    this.client.disconnect();
  }

  public async subscribe(channels: FeedChannel[]) {
    this.validateChannels(channels);
    return this.send({
      type: 'subscribe',
      channels: this.convertToApiChannels(channels)
    });
  }

  public async unsubscribe(channels: FeedChannel[]) {
    this.validateChannels(channels);
    return this.send({
      type: 'unsubscribe',
      channels: this.convertToApiChannels(channels)
    });
  }

  private validateChannels(channels: FeedChannel[]): void {
    ow(
      channels,
      'channels',
      ow.array.ofType(ow.object.exactShape(feedChannelShape))
    );
  }

  private convertToApiChannels(channels: FeedChannel[]) {
    return channels.map(channel =>
      omitBy(isUndefined, {
        name: channel.name,
        product_id: (channel as any).productId
      })
    );
  }

  public async placeOrder(placement: OrderToPlace) {
    if (placement.key) {
      validateOrderPlacement(placement);
      const preparation = await this.prepareOrder(placement);
      return this.sendPlaceOrder(preparation);
    } else {
      validateUnEncryptedOrderPlacement(placement);
      const preparation = await this.prepareUnEncryptedOrder(placement);
      return this.sendPlaceOrder(preparation);
    }
  }

  public async cancelOrder(cancellation: OrderCancellation) {
    if (cancellation.key) {
      validateOrderCancellation(cancellation);
      const preparation = await this.prepareOrderCancellation(cancellation);
      if (!preparation) {
        return null;
      }
      return this.sendPlaceOrder(preparation);
    } else {
      validateUnencryptedOrderCancellation(cancellation);
      const preparation = await this.prepareUnencryptedOrderCancellation(
        cancellation
      );
      if (!preparation) {
        return null;
      }
      return this.sendCancelOrder(preparation);
    }
  }

  private sendPlaceOrder(
    preparation: OrderPrepared | UnEncryptedOrderPrepared
  ) {
    if (preparation.hasOwnProperty('keyId')) {
      const castedPreparation = preparation as OrderPrepared;
      const { keyId, order, reference, signature } = castedPreparation;
      return this.send({
        type: 'placeorder',
        order: { key_id: keyId, order, reference, signature }
      });
    } else {
      return this.send({
        type: 'placeunencryptedorder',
        payload: preparation
      });
    }
  }

  private sendCancelOrder(preparedCancelOrder: CancellationPrepared) {
    return this.send({
      type: 'cancelorder',
      payload: preparedCancelOrder
    });
  }

  private send(data: any) {
    return this.client.send(JSON.stringify(data));
  }
}
