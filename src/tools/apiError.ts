export enum ErrorCode {
  unknown = 'UNKNOWN',
  unauthorized = 'UNAUTHORIZED',
  notFound = 'NOT_FOUND'
}

export interface ApiErrorData {
  code: string;
  [key: string]: any;
}

export class ApiError extends Error {
  public readonly data: ApiErrorData;

  constructor(message: string, data: ApiErrorData) {
    super();
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.data = data;
    this.message = `${message} (code: ${data.code})`;
  }
}
