import * as moment from 'moment';

export type ApiOhlcv = [
  string,
  string | null,
  string | null,
  string | null,
  string | null,
  string | null
];

export interface ApiCandles {
  prices: ApiOhlcv[];
}

export type OhlcvValue = number | null;

export interface Ohlcv {
  time: Date;
  open: OhlcvValue;
  close: OhlcvValue;
  high: OhlcvValue;
  low: OhlcvValue;
  volume: OhlcvValue;
}

export interface Candles {
  prices: Ohlcv[];
}

export function convertApiCandles(apiCandles: ApiCandles): Candles {
  return {
    prices: apiCandles.prices.map(convertApiOhlcv)
  };
}

function convertApiOhlcv(value: ApiOhlcv): Ohlcv {
  const [time, low, high, open, close, volume] = value;
  return {
    time: moment(time).toDate(),
    low: floatOrNull(low),
    high: floatOrNull(high),
    open: floatOrNull(open),
    close: floatOrNull(close),
    volume: floatOrNull(volume)
  };
}

function floatOrNull(floatMaybe: string | null) {
  if (!floatMaybe) {
    return null;
  }
  return Number.parseFloat(floatMaybe);
}
