import { OrderToPlace, OrderType } from './types';

export function orderToCSV(order: OrderToPlace, timestamp: number) {
  const parts = [
    order.type,
    order.side,
    order.productId,
    order.quantity,
    order.price,
    order.type === OrderType.market ? '' : 'gtc',
    timestamp
  ];
  return parts.join(',');
}

export function orderCancellationToCSV(orderId: string, timestamp: number) {
  const parts = ['C', orderId, timestamp];
  return parts.join(',');
}
