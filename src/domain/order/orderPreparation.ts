import ow from 'ow';
import { CreateUtcDate, CryptoKi } from '../../tools';

import { keyShape } from '../key';
import { EncryptOrder } from './orderEncryption';
import { OrderPrepared, OrderToPlace } from './types';

interface Dependencies {
  createUtcDate: CreateUtcDate;
  cryptoKi: CryptoKi;
  encryptOrder: EncryptOrder;
}

export type PrepareOrder = (placement: OrderToPlace) => Promise<OrderPrepared>;

export function createPrepareOrder(dependencies: Dependencies): PrepareOrder {
  const { createUtcDate, cryptoKi, encryptOrder } = dependencies;

  return async (placement: OrderToPlace) => {
    const {
      key,
      price,
      productId,
      quantity,
      reference,
      side,
      type
    } = placement;
    if (!key) {
      throw new Error('Cannot prepare order whithout key');
    }
    const orderToPlace = { type, side, productId, quantity, price };
    const encryptedOrder = await encryptOrder(
      key,
      orderToPlace,
      Math.floor(createUtcDate().getTime() / 1000)
    );
    const signedOrder = await cryptoKi.sign(encryptedOrder);
    const signature = {
      source: 'RSA',
      value: signedOrder
    };
    return {
      keyId: key.id,
      order: encryptedOrder,
      signature,
      reference
    };
  };
}

export function validateOrderPlacement(placement: OrderToPlace): void {
  ow(
    placement,
    'placement',
    ow.object.exactShape({
      type: ow.string,
      side: ow.string,
      productId: ow.string,
      quantity: ow.number,
      price: ow.optional.number,
      reference: ow.optional.number,
      key: keyShape
    })
  );
}
