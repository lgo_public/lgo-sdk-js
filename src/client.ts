/* tslint:disable:no-var-requires */
import { mergeAll } from 'lodash/fp';
import ow from 'ow';

const pk = require('../package.json');
import { Configuration, envConfigurations, Environment } from './configuration';
import {
  createPrepareOrder,
  createPrepareOrderCancellation,
  createPrepareUnEcryptedOrder,
  createPrepareUnencryptedOrderCancellation,
  createSelectKey,
  encryptOrder,
  encryptOrderCancellation,
  SelectKey
} from './domain';
import {
  createAxiosForApi,
  createAxiosForExternal,
  createHeadersFactory,
  createSilentLogger,
  createUtcDate,
  CryptoKi,
  Logger,
  WebSocketClient
} from './tools';
import {
  CancelOrder,
  createCancelOrder,
  createGetCandles,
  createGetCurrencies,
  createGetKeys, createGetMyOpenOrders,
  createGetMyOperations,
  createGetMyOrder,
  createGetMyOrders,
  createGetMyTrades,
  createGetOrderBookL1,
  createGetOrderBookL2,
  createGetProducts,
  createGetProductTrades,
  createPlaceOrder,
  GetCandles,
  GetCurrencies,
  GetKeys, GetMyOpenOrders,
  GetMyOperations,
  GetMyOrder,
  GetMyOrders,
  GetMyTrades,
  GetOrderBookL1,
  GetOrderBookL2,
  GetProducts,
  GetProductTrades,
  PlaceOrder,
  WebSocketFacade
} from './useCases';
import { createGetMyBalances, GetMyBalances } from './useCases/balances';

export interface ClientOptions extends Partial<Configuration> {
  cryptoKi: CryptoKi;
  env?: Environment;
  logger?: Logger;
  accessKey: string;
}

export class Client {
  public getCurrencies: GetCurrencies;
  public getOrderBookL1: GetOrderBookL1;
  public getOrderBookL2: GetOrderBookL2;
  public getProducts: GetProducts;
  public getCandles: GetCandles;
  public getMyTrades: GetMyTrades;
  public getMyOrder: GetMyOrder;
  public getMyOrders: GetMyOrders;
  public getTrades: GetProductTrades;
  public getKeys: GetKeys;
  public placeOrder: PlaceOrder;
  public cancelOrder: CancelOrder;
  public getMyOperations: GetMyOperations;
  public webSocket: WebSocketFacade;
  public selectKey: SelectKey;
  public getMyOpenOrders: GetMyOpenOrders;
  public getMyBalances: GetMyBalances;

  constructor(options: ClientOptions) {
    this.validateOptions(options);
    const sdkVersion = `${pk.name} ${pk.version}`;
    const { cryptoKi, env, logger, ...configuration } = this.parseOptions(
      options
    );
    const createHeaders = createHeadersFactory({
      configuration,
      createUtcDate,
      cryptoKi,
      sdkVersion
    });
    const apiAxios = createAxiosForApi({ createHeaders });
    const axios = createAxiosForExternal();
    const webSocketClient = new WebSocketClient(
      Object.assign({}, configuration.webSocket, {
        createHeaders,
        url: configuration.wsGatewayUrl,
        logger
      })
    );
    const prepareOrder = createPrepareOrder({
      cryptoKi,
      createUtcDate,
      encryptOrder
    });

    const prepareUnEncryptedOrder = createPrepareUnEcryptedOrder({
      cryptoKi,
      createUtcDate
    });

    const prepareOrderCancellation = createPrepareOrderCancellation({
      cryptoKi,
      createUtcDate,
      encryptOrderCancellation
    });

    const prepareUnencryptedOrderCancellation = createPrepareUnencryptedOrderCancellation(
      {
        cryptoKi,
        createUtcDate
      }
    );

    this.webSocket = new WebSocketFacade({
      client: webSocketClient,
      prepareOrder,
      prepareOrderCancellation,
      prepareUnencryptedOrder: prepareUnEncryptedOrder,
      prepareUnencryptedOrderCancellation
    });
    this.getCurrencies = createGetCurrencies({
      axios: apiAxios,
      configuration
    });
    this.getOrderBookL1 = createGetOrderBookL1({
      axios: apiAxios,
      configuration
    });
    this.getOrderBookL2 = createGetOrderBookL2({
      axios: apiAxios,
      configuration
    });
    this.getTrades = createGetProductTrades({
      axios: apiAxios,
      configuration
    });
    this.getProducts = createGetProducts({
      axios: apiAxios,
      configuration
    });
    this.getCandles = createGetCandles({
      axios: apiAxios,
      configuration
    });
    this.getMyTrades = createGetMyTrades({
      axios: apiAxios,
      configuration
    });
    this.getMyOrder = createGetMyOrder({
      axios: apiAxios,
      configuration
    });
    this.getMyOrders = createGetMyOrders({
      axios: apiAxios,
      configuration
    });
    this.getMyOpenOrders = createGetMyOpenOrders({
      axios: apiAxios,
      configuration
    });
    this.getKeys = createGetKeys({
      axios,
      createUtcDate,
      configuration
    });
    this.placeOrder = createPlaceOrder({
      axios: apiAxios,
      configuration,
      prepareOrder
    });
    this.cancelOrder = createCancelOrder({
      axios: apiAxios,
      configuration,
      prepareOrderCancellation
    });
    this.getMyOperations = createGetMyOperations({
      axios: apiAxios,
      configuration
    });
    this.selectKey = createSelectKey({ createUtcDate });
    this.getMyBalances = createGetMyBalances({
      axios: apiAxios,
      configuration
    })
  }

  private validateOptions(options: ClientOptions): void {
    ow(
      options,
      'options',
      ow.object.exactShape({
        accessKey: ow.string,
        cryptoKi: ow.object.partialShape({
          sign: ow.function
        }),
        env: ow.optional.string,
        logger: ow.optional.object.partialShape({
          log: ow.function,
          error: ow.function
        }),
        exchangeUrl: ow.optional.string,
        accountingUrl: ow.optional.string,
        wsGatewayUrl: ow.optional.string,
        keysUrl: ow.optional.string,
        webSocket: ow.optional.object.partialShape({
          connectionTimeoutDelay: ow.optional.number,
          reconnectionDelay: ow.optional.number,
          pingDelay: ow.optional.number,
          pongTimeoutDelay: ow.optional.number
        })
      })
    );
  }

  private parseOptions(options: ClientOptions): Required<ClientOptions> {
    const { env = 'production', ...userOptions } = options;
    return mergeAll([
      {
        logger: createSilentLogger(),
        webSocket: {
          connectionTimeoutDelay: 10000,
          reconnectionDelay: 3000,
          pingDelay: 5000,
          pongTimeoutDelay: 10000
        },
        env
      },
      envConfigurations[env],
      userOptions
    ]);
  }
}
