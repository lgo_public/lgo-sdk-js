import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

import { CreateHeaders } from './headers';
import { wrapAxiosError } from './httpErrors';

interface Dependencies {
  createHeaders: CreateHeaders;
}

export function createAxiosForExternal(): AxiosInstance {
  const axios = Axios.create();
  axios.interceptors.response.use(r => r, wrapResponseError);
  return axios;
}

export function createAxiosForApi(dependencies: Dependencies): AxiosInstance {
  const { createHeaders } = dependencies;
  const axios = Axios.create();
  axios.interceptors.request.use(addHeadersForApi);
  axios.interceptors.response.use(r => r, wrapResponseError);
  return axios;

  async function addHeadersForApi(config: AxiosRequestConfig) {
    const apiHeaders = config.data
      ? await createHeaders(config.url || '', config.data)
      : await createHeaders(config.url || '');
    const headers = Object.assign({}, config.headers, apiHeaders);
    return Object.assign({}, config, { headers });
  }
}

function wrapResponseError(error: Error) {
  return Promise.reject(wrapAxiosError(error));
}
