import { ErrorCode } from '../apiError';
import { wrapAxiosError as wrapHttpError } from './httpErrors';

describe('Wrapping http error', () => {
  describe('should return an api error', () => {
    it('and reuse response error information', () => {
      const wrappedError = Object.assign(new Error('Something failed'), {
        response: {
          status: 480,
          data: { code: 'BLEH' }
        }
      });

      const error = wrapHttpError(wrappedError);

      expect(error.message).toEqual('Request failed (code: BLEH)');
      expect(error.data.code).toEqual('BLEH');
      expect(error.data.httpCode).toEqual(480);
    });

    it('and reuse generic error information', () => {
      const wrappedError = Object.assign(new Error('Something failed'));

      const error = wrapHttpError(wrappedError);

      expect(error.message).toEqual('Request failed (code: UNKNOWN)');
      expect(error.data.code).toEqual(ErrorCode.unknown);
      expect(error.data.httpCode).toBeUndefined();
    });

    it('and reuse provided code though response status is known', () => {
      const error = wrapHttpError(
        Object.assign(new Error('Something failed'), {
          response: {
            status: 404,
            data: { code: 'BLEH' }
          }
        })
      );

      expect(error.data.code).toEqual('BLEH');
    });

    it('and use unknown code if missing in provided error', () => {
      const error = wrapHttpError(new Error('bleh'));

      expect(error.data.code).toEqual(ErrorCode.unknown);
    });

    it('and use unauthorized code if response status is 401', () => {
      const error = wrapHttpError(
        Object.assign(new Error('Something failed'), {
          response: {
            status: 401
          }
        })
      );

      expect(error.data.code).toEqual(ErrorCode.unauthorized);
    });

    it('and use not found code if response status is 404', () => {
      const error = wrapHttpError(
        Object.assign(new Error('Something failed'), {
          response: {
            status: 404
          }
        })
      );

      expect(error.data.code).toEqual(ErrorCode.notFound);
    });
  });
});
