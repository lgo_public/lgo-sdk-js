import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import { PrepareOrder } from '../../../../domain';
import { AxiosMock, createKey, createOrderToPlace } from '../../../../test';
import { createPlaceOrder, PlaceOrder } from './placeOrder';

describe('Place order', () => {
  let axios: AxiosInstance;
  let placeOrder: PlaceOrder;
  let prepareOrder: PrepareOrder;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    prepareOrder = jest.fn().mockResolvedValue(undefined);
    axios.post = jest.fn().mockResolvedValue({ data: { order_id: '1234' } });
    placeOrder = createPlaceOrder({
      configuration,
      axios,
      prepareOrder
    });
  });

  describe('encrypted', () => {
    it('should prepare order then post it to corresponding resource', async () => {
      const order = createOrderToPlace();
      const key = createKey({ id: '1337' });
      (prepareOrder as any).mockResolvedValue({
        keyId: '1337',
        order: 'encrypted',
        signature: { source: 'RSA', value: 'signed' },
        reference: 42
      });

      await placeOrder({
        ...order,
        key,
        reference: 42
      });

      const expectedUrl = 'http://exchange/v1/live/orders/encrypted';
      const expectedBody = {
        key_id: '1337',
        order: 'encrypted',
        signature: {
          source: 'RSA',
          value: 'signed'
        },
        reference: 42
      };
      expect(axios.post).toHaveBeenCalledWith(expectedUrl, expectedBody);
    });

    it('should reject if placement is malformed', async () => {
      const key = createKey({ id: '1337' });

      const call = placeOrder({
        ...createOrderToPlace({ type: 3 } as any),
        key,
        reference: 42
      } as any);

      await expect(call).rejects.toThrow(
        'Expected property `type` to be of type `string` but received type `number`'
      );
    });
  });

  describe('unencrypted', () => {
    it('should post an order to corresponding resource', async () => {
      const order = createOrderToPlace();
      await placeOrder(order);
      const expectedUrl = 'http://exchange/v1/live/orders';
      const expectedBody = {
        type: order.type,
        side: order.side,
        product_id: order.productId,
        quantity: order.quantity,
        price: order.price,
        reference: order.reference
      };
      expect(axios.post).toHaveBeenCalledWith(expectedUrl, expectedBody);
    });
  });
});
