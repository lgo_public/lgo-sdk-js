import * as moment from 'moment';

import { OrderSide } from '../../../domain';
import { ApiEntityPage, convertApiPage, EntityPage } from '../../../tools';

export interface ApiUserTrade {
  id: string;
  order_id: string;
  product_id: string;
  price: string;
  fees: string;
  quantity: string;
  creation_date: string;
  side: OrderSide;
}

export interface ApiUserTradePage extends ApiEntityPage<ApiUserTrade> {
  result: {
    trades: ApiUserTrade[];
  };
}

export interface UserTrade {
  id: string;
  productId: string;
  price: number;
  quantity: number;
  fees: number;
  orderId: string;
  creationDate: Date;
  side: OrderSide;
}

export type UserTradePage = EntityPage<UserTrade>;

export function convertApiUserTradePage(
  apiUserTradePage: ApiUserTradePage
): UserTradePage {
  return convertApiPage(
    apiUserTradePage,
    p => p.result.trades,
    convertApiUserTrade
  );
}

function convertApiUserTrade(apiUserTrade: ApiUserTrade): UserTrade {
  return {
    id: apiUserTrade.id,
    productId: apiUserTrade.product_id,
    quantity: Number.parseFloat(apiUserTrade.quantity),
    price: Number.parseFloat(apiUserTrade.price),
    fees: Number.parseFloat(apiUserTrade.fees),
    creationDate: moment.utc(apiUserTrade.creation_date).toDate(),
    side: apiUserTrade.side,
    orderId: apiUserTrade.order_id
  };
}
