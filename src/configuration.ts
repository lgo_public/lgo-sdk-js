export type Environment =
  | 'local'
  | 'production'
  | 'sandbox'
  | 'markets-production'
  | 'markets-sandbox'
  | 'exchange-production'
  | 'exchange-sandbox';

export interface EnvConfiguration {
  exchangeUrl: string;
  accountingUrl: string;
  wsGatewayUrl: string;
  keysUrl: string;
}

type EnvConfigurations = { [key in Environment]: EnvConfiguration };

const production: EnvConfiguration = {
  exchangeUrl: 'https://exchange-api.exchange.lgo.markets',
  accountingUrl: 'https://account-api.exchange.lgo.markets',
  wsGatewayUrl: 'wss://ws.exchange.lgo.markets',
  keysUrl: 'https://storage.googleapis.com/lgo-markets-keys'
};

const sandbox: EnvConfiguration = {
  exchangeUrl: 'https://exchange-api.sandbox.lgo.markets',
  accountingUrl: 'https://account-api.sandbox.lgo.markets',
  wsGatewayUrl: 'wss://ws.sandbox.lgo.markets',
  keysUrl: 'https://storage.googleapis.com/lgo-sandbox_batch_keys'
};

export const envConfigurations: EnvConfigurations = {
  local: {
    exchangeUrl: 'http://localhost:8081/v1',
    accountingUrl: 'http://localhost:8087',
    wsGatewayUrl: 'ws://localhost:8084',
    keysUrl: 'http://localhost:3001/keys'
  },
  production,
  sandbox,
  'markets-sandbox': sandbox,
  'markets-production': production,
  'exchange-sandbox': sandbox,
  'exchange-production': production
};

export interface WebSocketConfiguration {
  webSocket: {
    connectionTimeoutDelay: number;
    reconnectionDelay: number;
    pingDelay: number;
    pongTimeoutDelay: number;
  };
}

export interface Configuration
  extends EnvConfiguration,
    WebSocketConfiguration {
  accessKey: string;
}
