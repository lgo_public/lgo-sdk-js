import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../configuration';
import { createUrl } from '../../../tools';
import {
  ApiMarketDataL1,
  convertApiMarketDataL1,
  MarketDataL1
} from './orderBook';

export interface GetOrderBookL1Query {
  productId: string;
}

export type GetOrderBookL1 = (
  query: GetOrderBookL1Query
) => Promise<MarketDataL1>;

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export function createGetOrderBookL1(
  dependencies: Dependencies
): GetOrderBookL1 {
  const { axios, configuration } = dependencies;

  return async (query: GetOrderBookL1Query) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        productId: ow.string
      })
    );
    const { productId } = query;
    const url = createUrl(
      `${configuration.exchangeUrl}/v1/live/products/${productId}/book`,
      { level: 1 }
    );
    const { data } = await axios.get<ApiMarketDataL1>(url);
    return convertApiMarketDataL1(data);
  };
}
