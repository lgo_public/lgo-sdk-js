import { AxiosInstance } from 'axios';

import { Configuration } from '../../../../configuration';
import { OrderStatus } from '../../../../domain';
import {
  AxiosMock,
  createApiUserOrder,
  createUserOrder
} from '../../../../test';
import { createGetMyOrders, GetMyOrders } from './getMyOrders';

describe('Get my orders', () => {
  let axios: AxiosInstance;
  let getMyOrders: GetMyOrders;

  beforeEach(() => {
    const configuration = { exchangeUrl: 'http://exchange' } as Configuration;
    axios = new AxiosMock();
    getMyOrders = createGetMyOrders({
      configuration,
      axios
    });
  });

  beforeEach(() => {
    axios.get = jest.fn().mockResolvedValue({
      data: {
        result: {
          orders: [
            createApiUserOrder({ id: '1' }),
            createApiUserOrder({ id: '2' })
          ]
        },
        next_page: 'page42'
      }
    });
  });

  it('should get corresponding resource', async () => {
    await getMyOrders();

    const expectedUrl = 'http://exchange/v1/history/orders';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should use provided query to get resource', async () => {
    await getMyOrders({
      productId: 'BTC-USD',
      status: OrderStatus.done,
      maxResults: 3,
      page: '42'
    });

    const expectedUrl =
      'http://exchange/v1/history/orders?max_results=3&page=42&product_id=BTC-USD&status=DONE';
    expect(axios.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should return page', async () => {
    const result = await getMyOrders();

    expect(result).toEqual({
      results: [createUserOrder({ id: '1' }), createUserOrder({ id: '2' })],
      nextPage: 'page42'
    });
  });

  it('should reject if product id is malformed', async () => {
    const call = getMyOrders({ productId: 3 } as any);

    await expect(call).rejects.toThrow(
      'Expected property `productId` to be of type `string` but received type `number`'
    );
  });

  it('should reject if pagination is malformed', async () => {
    const call = getMyOrders({ maxResults: 'help me' } as any);

    await expect(call).rejects.toThrow(
      'Expected property `maxResults` to be of type `number` but received type `string`'
    );
  });
});
