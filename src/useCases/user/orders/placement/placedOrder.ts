export interface ApiPlacedOrder {
  order_id: string;
}
export interface PlacedOrder {
  orderId: string;
}

export function convertToPlacedOrder(placedOrder: ApiPlacedOrder): PlacedOrder {
  return { orderId: placedOrder.order_id };
}
