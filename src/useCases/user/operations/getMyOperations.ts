import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../configuration';
import {
  convertPaginationQueryToApi,
  createUrl,
  PaginationQuery,
  paginationShape
} from '../../../tools';
import {
  ApiUserOperationPage,
  convertApiUserOperationPage,
  OperationType,
  UserOperationPage
} from './userOperations';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetMyOperationsQuery extends PaginationQuery {
  currencyCode?: string;
  type?: OperationType;
}

export type GetMyOperations = (
  query?: GetMyOperationsQuery
) => Promise<UserOperationPage>;

export function createGetMyOperations(
  dependencies: Dependencies
): GetMyOperations {
  const { axios, configuration } = dependencies;

  return async (query: GetMyOperationsQuery = {}) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        ...paginationShape,
        currencyCode: ow.optional.string,
        type: ow.optional.string
      })
    );
    const apiQuery = Object.assign(convertPaginationQueryToApi(query), {
      currency_code: query.currencyCode,
      type: query.type
    });
    const { data } = await axios.get<ApiUserOperationPage>(
      createUrl(`${configuration.accountingUrl}/operations`, apiQuery)
    );
    return convertApiUserOperationPage(data);
  };
}
