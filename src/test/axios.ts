import { AxiosInstance } from 'axios';

import { mock } from './mock';

export const AxiosMock = mock<AxiosInstance>(() => ({
  get: jest.fn().mockResolvedValue({ data: {} }),
  put: jest.fn().mockResolvedValue({ data: {} }),
  post: jest.fn().mockResolvedValue({ data: {} }),
  delete: jest.fn().mockResolvedValue({ data: {} })
}));
