export function mock<T>(constructor: () => Partial<T>) {
  return jest.fn(() => (constructor() as any) as T);
}
