import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../configuration';
import {
  convertPaginationQueryToApi,
  createUrl,
  PaginationQuery,
  paginationShape
} from '../../../tools';
import {
  ApiUserTradePage,
  convertApiUserTradePage,
  UserTradePage
} from './userTrades';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetMyTradesQuery extends PaginationQuery {
  productId?: string;
}

export type GetMyTrades = (query?: GetMyTradesQuery) => Promise<UserTradePage>;

export function createGetMyTrades(dependencies: Dependencies): GetMyTrades {
  const { axios, configuration } = dependencies;

  return async (query: GetMyTradesQuery = {}) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        ...paginationShape,
        productId: ow.optional.string
      })
    );
    const apiQuery = Object.assign(convertPaginationQueryToApi(query), {
      product_id: query.productId
    });
    const { data } = await axios.get<ApiUserTradePage>(
      createUrl(`${configuration.exchangeUrl}/v1/history/trades`, apiQuery)
    );
    return convertApiUserTradePage(data);
  };
}
