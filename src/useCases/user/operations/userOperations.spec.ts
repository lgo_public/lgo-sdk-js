import { createApiUserOperation, createUserOperation } from '../../../test';
import { convertApiUserOperationPage } from './userOperations';

describe('User operations', () => {
  describe('while converting api user operation page', () => {
    it('should return data', () => {
      const apiPage = {
        result: {
          operations: [
            createApiUserOperation({ id: '1' }),
            createApiUserOperation({ id: '2' })
          ]
        },
        next_page: 'page42'
      };

      const page = convertApiUserOperationPage(apiPage);

      expect(page).toEqual({
        results: [
          createUserOperation({ id: '1' }),
          createUserOperation({ id: '2' })
        ],
        nextPage: 'page42'
      });
    });
  });
});
