# Client creation

To use the library instanciate a new `Client`.

```typescript
import { Client } from '@lgo/sdk';

const client = new Client(/* options */);
```

You can also use require syntax:

```javascript
const { Client } = require('@lgo/sdk');
```

Options are provided as an `object` with following properties:

- `cryptoKi: CryptoKi` - A CryptoKi compatible implementation. See [CryptoKi](#cryptoki).
- `env?: string` - The target environment. Defaults to `production`. See [Environments](#environments).
- `logger?: Logger` - A logger compatible implementation. Logs nothing by default. See [Logging](#logging).
- `accessKey: string` - Your personal access key on LGO backend.
- `webSocket?: {}` - WebSocket specific options.
  - `connectionTimeoutDelay?: number` - Delay to wait in ms before throwing an error if connection takes too much time. Defaults to `10000` ms.
  - `reconnectionDelay?: number` - Delay to wait in ms before trying to reconnect. Defaults to `3000` ms.
  - `pingDelay?: number` - Delay to wait in ms before sending each ping requests. Defaults to `5000` ms.
  - `pongTimeoutDelay?: number` - Delay to wait in ms before detecting a missing pong response and then forcing disconnection. Defaults to `10000` ms.

Options for advanced users or edge cases:

- `exchangeUrl: string` - Overwrites the exchange backend url.
- `accountingUrl: string` - Overwrites the accounting backend url.
- `wsGatewayUrl: string` - Overwrites the WebSocket gateway backend url.
- `keysUrl: string` - Overwrites the encryption keys host url.

Example:

```typescript
const client = new Client({
  cryptoKi: myCryptoKi,
  accessKey: '971432e9-746c-4c29-ab68-24704347a1e3',
  webSocket: {
    connectionTimeoutDelay: 30000
  }
});
```

# CryptoKi

You must provide a `CryptoKi` compatible implementation to the sdk.

The interface must be:

```typescript
export interface CryptoKi {
  sign: (data: string) => Promise<string>;
}
```

The resulting signature must be a encoded to hexadecimal.

Available implementations:

- [@lgo/sdk-localrsa](https://gitlab.com/lgo_public/lgo-sdk-js-localrsa): implementation based on a RSA key pair you own. Easier solution.
- [@lgo/sdk-softhsm](https://gitlab.com/lgo_public/lgo-sdk-js-softhsm): implementation based on SoftHSM.

# Environments

LGO sdk can be configured to target LGO Markets or LGO Exchange with their own production or sandbox environments.

Available environments:

| Name       | Description                                             |
| ---------- | ------------------------------------------------------- |
| production | LGO production environment. Default environment.        |
| sandbox    | LGO sandbox environment to test api without real money. |

Depecrated environments:

| Name                | Description              |
| ------------------- | ------------------------ |
| markets-production  | Equivalent to production |
| markets-sandbox     | Equivalent to sandbox    |
| exchange-production | Equivalent to production |
| exchange-sandbox    | Equivalent to sandbox    |

Example:

```typescript
const client = new Client({
  env: 'markets-sandbox'
  /* ... other options */
});
```

# Logging

You can provide a `Logger` compatible implementation and the sdk will use it to display information or error messages.

The signature must be:

```typescript
interface Logger {
  log: (message: string) => void;
  error: (message: string) => void;
}
```

An obvious implementation is Node.js `console` object.

Example:

```typescript
const client = new Client({
  logger: console
  /* ... other options */
});
```

# HTTP use cases

## Get available currencies

`client.getCurrencies(): Promise<{}>`

- returns: a promise of an object containing currencies.

Example:

```typescript
client.getCurrencies().then(currencies => {
  // ...
});
```

Output example:

```console
{
  currencies: [
    { name: 'Bitcoin', code: 'BTC', decimals: 8 },
    { name: 'United States Dollar', code: 'USD', decimals: 4 }
  ];
}
```

## Get available products

`client.getProducts(): Promise<{}>`

- returns: a promise of an object containing products.

Example:

```typescript
client.getProducts().then(products => {
  // ...
});
```

Output example:

```console
{
  products: [
    {
      id: 'BTC-USD',
      base: {
        id: 'BTC',
        limits: { min: 0.001, max: 1000 }
      },
      quote: {
        id: 'USD',
        increment: 0.1,
        limits: { min: 10, max: 1000000 }
      },
      total: {
        limits: { min: 10, max: 50000000 }
      }
    }
  ];
}
```

## Get order book level 1

`client.getOrderBookL1(query: {}): Promise<{}>`

- `query: {}` - The query to filter order book. Properties are:
  - `productId: string` - The product id to retrieve order book for. Example: `BTC-USD`.
- returns: a promise of an object containing order book.

Example:

```typescript
client.getOrderBookL1({ productId: 'BTC-USD' }).then(orderBook => {
  // ...
});
```

Output example:

```console
{
  productId: 'BTC-USD',
  bestBid: { price: 7998.3, quantity: 0.9658 },
  bestAsk: { price: 8000.5, quantity: 10.2507 }
}
```

## Get order book level 2

`client.getOrderBookL2(query: {}): Promise<{}>`

- `query: {}` - The query to filter order book. Properties are:
  - `productId: string` - The product id to retrieve order book for. Example: `BTC-USD`.
- returns: a promise of an object containing order book.

Example:

```typescript
client.getOrderBookL2({ productId: 'BTC-USD' }).then(orderBook => {
  // ...
});
```

Output example:

```console
{
  productId: 'BTC-USD',
  bids: [
    { price: 7998.3, quantity: 6.2147 },
    { price: 7998.1, quantity: 2.1442 },
    { price: 7997.8, quantity: 6.0854 },
    // ...
  ],
  asks: [
    { price: 8000.5, quantity: 1.3497 },
    { price: 8000.9, quantity: 8.1629 },
    { price: 8001.2, quantity: 1.1622 },
    // ...
  ]
}
```

## Get product trades

`client.getTrades(query: {}): Promise<{}>`

- `query: {}` - The query to filter order book. Properties are:
  - `productId: string` - The product id to retrieve trades for. Example: `BTC-USD`.
  - `maxResults?: number` - The maximum results to retrieve.
  - `page?: string` - The page to load. The next page is sent with result if any.
  - `sort?: string` - Sort direction. `ASC` for ascending or `DESC` for descending. Defaults to `DESC`.
- returns: a promise of an object containing trades.

Example:

```typescript
client.getTrades({ productId: 'BTC-USD', maxResults: 3 }).then(trades => {
  // ...
});
```

Output example:

```console
{
  results: [
    {
      id: '2002581',
      productId: 'BTC-USD',
      quantity: 0.6031,
      price: 8602.9,
      creationDate: 2019-05-29T13:03:15.742Z,
      side: 'B'
    },
    // ...
  ],
  nextPage: 'MjAwMjU3OQ=='
};
```

## Get price history as candles

`client.getCandles(query: {}): Promise<{}>`

- `query: {}` - The query to filter price history. Properties are:
  - `productId: string` - The product id to price history for. Example: `BTC-USD`.
  - `start: date` - The start date (included).
  - `end: date` - The end date (excluded).
  - `granularity: number` - Duration in seconds of price aggregations.
- returns: a promise of an object containing prices as OHLCV values.

(OHLCV: Open High Low Close Volume)

Available granularities:

| Value | Human readable value |
| ----- | -------------------- |
| 60    | 1 minute             |
| 300   | 5 minutes            |
| 900   | 15 minutes           |
| 3600  | 1 hour               |
| 21600 | 6 hours              |
| 86400 | 1 day                |

So if you query with a range corresponding to a full year and a granularity equals to 1 day, you will get 365 OHLCV values. Note that we limit to 400 values at most.

Example:

```typescript
client
  .getCandles({
    productId: 'BTC-USD',
    start: new Date('2019-05-01'),
    end: new Date('2019-05-04'),
    granularity: 86400
  })
  .then(prices => {
    // ...
  });
```

Output example:

```console
{
  prices: [
    {
      time: 2019-05-01T00:00:00.000Z,
      low: 5181.3,
      high: 5219.3,
      open: 5206.4,
      close: 5191.5,
      volume: 63607.356
    },
    {
      time: 2019-05-02T00:00:00.000Z,
      low: 5181.3,
      high: 5219.3,
      open: 5193.9,
      close: 5182.8,
      volume: 62729
    },
    {
      time: 2019-05-03T00:00:00.000Z,
      low: 5181.5,
      high: 5219.4,
      open: 5188.7,
      close: 5192.6,
      volume: 62307.9458
    }
  ]
}
```

## Get my orders

`client.getMyOrders(query: {}): Promise<{}>`

- `query: {}` - The query to filter orders. Properties are:
  - `productId?: string` - The product id to retrieve orders for. Example: `BTC-USD`.
  - `status?: string` - The status to filter orders. Use `OrderStatus` enum. Example: `OrderStatus.done`.
  - `maxResults?: number` - The maximum results to retrieve.
  - `page?: string` - The page to load. The next page is sent with result if any.
  - `sort?: string` - Sort direction. `ASC` for ascending or `DESC` for descending. Defaults to `DESC`.
- returns: a promise of an object containing user's orders.

Example:

```typescript
client.getMyOrders({ productId: 'BTC-USD', maxResults: 3 }).then(orders => {
  // ...
});
```

Output example:

```console
{
  results: [
    {
      id: '155853503445100001',
      type: 'L',
      side: 'S',
      status: 'OPEN',
      productId: 'BTC-USD',
      quantity: 0.1,
      remainingQuantity: 0.1,
      price: 8000,
      creationDate: 2019-05-22T14:23:54.451Z
    },
    // ...
  ],
  nextPage: 'MTU1ODUzMzE0NDkxMjAwMDAx'
}
```

## Get one of my orders

`client.getMyOrder(query: {}): Promise<{}>`

- `query: {}` - The query to filter order book. Properties are:
  - `id: string` - The order's id to load.
- returns: a promise of an object containing user's order.

Example:

```typescript
client.getMyOrder({ id: '155247507925500001' }).then(order => {
  // ...
});
```

Output example:

```console
{
  id: '155247507925500001',
  type: 'L',
  side: 'S',
  status: 'OPEN',
  productId: 'BTC-USD',
  quantity: 0.1,
  remainingQuantity: 0.1,
  price: 8000,
  creationDate: 2019-05-22T14:23:54.451Z
}
```

## Place an order

`client.placeOrder(order: {}): Promise<void>`

- `order: {}` - The order to place. Properties are:
  - `type: string` - `L` for limit or `M` for market.
  - `side: string` - `B` for buy or `S` for sell.
  - `productId: string` - The product id. Example: `BTC-USD`·.
  - `quantity: number` - The quantity. Example: `0.05`.
  - `price?: number` - The price. Not available for a market order. Example: `8000.1`.
  - `reference?: number` - A reference to track your order on `afr` WebSocket channel.
  - `key: {}` - A key object returned by `client.getKeys` to encrypt the order.
- returns: a promise containing nothing.

Example:

```typescript
import { Client, OrderType, OrderSide } from '@lgo/sdk';

const client = new Client(/* options */);

const keys = await client.getKeys();
const key = client.selectKey(keys);

const order = {
  type: OrderType.limit,
  side: OrderSide.sell,
  productId: 'BTC-USD',
  quantity: 1,
  price: 8000,
  key,
  reference: Date.now()
};

client.placeOrder(order).then(() => console.log('done'));
```

## Cancel an order

`client.cancelOrder(cancellation: {}): Promise<void>`

- `cancellation: {}` - The cancellation description. Properties are:
  - `orderId: string` - The id of the order to cancel.
  - `reference?: number` - A reference to track your order on `afr` WebSocket channel.
  - `key: {}` - A key object returned by `client.getKeys` to encrypt the order.
- returns: a promise containing nothing.

Example:

```typescript
import { Client } from '@lgo/sdk';

const client = new Client(/* options */);

const keys = await client.getKeys();
const key = client.selectKey(keys);

const cancellation = {
  key,
  reference: Date.now(),
  orderId: '155663606242700001'
};

client.cancelOrder(cancellation).then(() => console.log('done'));
```

## Get my trades

`client.getMyTrades(query: {}): Promise<{}>`

- `query: {}` - The query to filter trades. Properties are:
  - `productId?: string` - The product id to retrieve trades for. Example: `BTC-USD`.
  - `maxResults?: number` - The maximum results to retrieve.
  - `page?: string` - The page to load. The next page is sent with result if any.
  - `sort?: string` - Sort direction. `ASC` for ascending or `DESC` for descending. Defaults to `DESC`.
- returns: a promise of an object containing user's trades.

Example:

```typescript
client.getMyTrades({ productId: 'BTC-USD', maxResults: 3 }).then(orders => {
  // ...
});
```

Output example:

```console
{
  results: [
    {
      id: '1385813',
      productId: 'BTC-USD',
      quantity: 1,
      price: 8200.6,
      fees: 2.6003,
      creationDate: 2019-04-30T14:24:26.713Z,
      side: 'S',
      liquidity: 'T',
      orderId: '155663426556000001'
    },
    // ...
  ],
  nextPage: 'NXA3ODUzMzE0NDpsMjAwMBCw'
}
```

## Get accounting operations

`client.getMyOperations(query: {}): Promise<{}>`

- `query?: {}` - The query to filter trades. Properties are:
  - `currencyCode?: string` - The currency code to retrieve operations for. Example: `BTC`.
  - `type?: string` - The type to filter operations for. Possible values are `DEPOSIT` or `WITHDRAWAL`.
- returns: a promise of an object containing user's operations.

Example:

```typescript
import { Client, OperationType } from '@lgo/sdk';

const client = new Client(/* options */);

client
  .getMyOperations({ type: OperationType.deposit, currencyCode: 'BTC' })
  .then(operations => {
    // ...
  });
```

Output example:

```console
{
  results: [
    {
      id: 'bab458d5-577c-4545-9f94-e54ed49ef970',
      accountId: 'b70230f2-8902-4394-b802-34a32fcda558',
      ownerId: 764088,
      type: 'DEPOSIT',
      status: 'RECONCILED',
      quantity: 100,
      currency: 'BTC',
      counterParty: 'something',
      createdAt: 2019-03-20T16:35:09.810Z,
      protocol: 'ONCHAIN'
    }
  ],
  nextPage: 'PYB8OFUzMzE0NDpsMjAwMBCw'
};
```

# WebSocket use cases

## Subscribe to afr channel (orders in transit)

Example:

```typescript
import { Client, Feed } from '@lgo/sdk';

const client = new Client(/* options */);

client.webSocket.on('message', console.log);
client.webSocket.on('open', () => ws.subscribe([{ name: Feed.afr }]));

client.webSocket.connect();
```

Update output example:

```console
{
  payload: [
    {
      time: '2018-11-12T09:41:34.232Z',
      type: 'received',
      order_id: '154201569423200001',
      reference: '12345678899'
    }
  ],
  type: 'update',
  channel: 'afr'
}
```

## Subscribe to user channel (orders status update)

Example:

```typescript
import { Client, Feed } from '@lgo/sdk';

const client = new Client(/* options */);

client.webSocket.on('message', console.log);
client.webSocket.on('open', () =>
  ws.subscribe([{ name: Feed.user, productId: 'BTC-USD' }])
);

client.webSocket.connect();
```

Snapshot output example:

```console
{
  payload: [
    {
      quantity: '1.00000000',
      order_creation_time: '2018-11-12T09:45:26.475Z',
      side: 'S',
      remaining_quantity: '1.00000000',
      order_id: '154201592647500001',
      order_type: 'L',
      price: '7000.0000'
    },
    {
      quantity: '1.00000000',
      side: 'S',
      remaining_quantity: '1.00000000',
      order_creation_time: '2018-11-12T10:24:54.091Z',
      order_id: '154201829409100001',
      order_type: 'L',
      price: '7000.0000'
    },
    {
      quantity: '1.00000000',
      side: 'S',
      remaining_quantity: '1.00000000',
      order_id: '154201569423200001',
      order_type: 'L',
      order_creation_time: '2018-11-12T09:41:34.232Z',
      price: '7000.0000'
    }
  ],
  batch_id: 793,
  type: 'snapshot',
  channel: 'user',
  product_id: 'BTC-USD'
}
```

Update output example:

```console
{
  payload: [
    {
      quantity: '1.00000000',
      time: '2018-11-12T10:24:54.091Z',
      side: 'S',
      order_id: '154201829409100001',
      type: 'pending',
      order_type: 'L',
      price: '7000.0000'
    },
    {
      order_id: '154201829409100001',
      time: '2018-11-12T10:24:57.104Z',
      type: 'open'
    },
    {
      price: '8394.2000',
      fees: '17.3424',
      time: '2018-11-12T14:11:33.006Z',
      trade_id: '2140',
      type: 'match',
      liquidity: 'M',
      order_id: '154203187584200001',
      filled_quantity: '4.13200000',
      remaining_quantity: '4.61240000'
    }
  ],
  batch_id: 794,
  type: 'update',
  channel: 'user',
  product_id: 'BTC-USD'
}
```

## Subscribe to order book level 2 channel

Example:

```typescript
import { Client, Feed } from '@lgo/sdk';

const client = new Client(/* options */);

client.webSocket.on('message', console.log);
client.webSocket.on('open', () =>
  ws.subscribe([{ name: Feed.level2, productId: 'BTC-USD' }])
);

client.webSocket.connect();
```

Snapshot output example:

```console
{
  payload: {
    bids: [
      ['7998.3000', '6.21470000'],
      ['7998.1000', '2.14420000'],
      ['7997.8000', '6.08540000']
    ],
    asks: [
      ['8000.5000', '1.34970000'],
      ['8000.9000', '8.16290000'],
      ['8001.2000', '1.16220000']
    ]
  },
  batch_id: 776,
  type: 'snapshot',
  channel: 'level2',
  product_id: 'BTC-USD'
}
```

Update output example:

```console
{
  payload: {
    bids: [],
    asks: [['7998.1000', '1.32120000']]
  },
  batch_id: 777,
  type: 'update',
  channel: 'level2',
  product_id: 'BTC-USD'
}
```

## Subscribe to my balance channel

Example:

```typescript
import { Client, Feed } from '@lgo/sdk';

const client = new Client(/* options */);

client.webSocket.on('message', console.log);
client.webSocket.on('open', () => {
  ws.subscribe([{ name: Feed.balance }]);
});

client.webSocket.connect();
```

Snapshot output example:

```console
{
  payload: [
    ['BTC', '2.76000000', '0.00000000'],
    ['USD', '1003.5912', '0.0000']
  ],
  seq: 3,
  type: 'snapshot',
  channel: 'balance'
}
```

Update output example:

```console
{
  seq: 3,
  payload: [['USD', '990.5912', '0.0000']],
  type: 'update',
  channel: 'balance'
}
```

## Subscribe to last LGO trades channel

Example:

```typescript
import { Client, Feed } from '@lgo/sdk';

const client = new Client(/* options */);

client.webSocket.on('message', console.log);
client.webSocket.on('open', () =>
  ws.subscribe([{ name: Feed.trades, productId: 'BTC-USD' }])
);

client.webSocket.connect();
```

Snapshot output example:

```console
{
  payload: [
    {
      quantity: '1.00000000',
      side: 'S',
      price: '8000.0000',
      trade_id: '222',
      trade_creation_time: '2018-11-22T14:26:24.636Z'
    }
  ],
  batch_id: 777,
  type: 'snapshot',
  channel: 'trades',
  product_id: 'BTC-USD'
}
```

Update output example:

```console
{
  payload: [
    {
      quantity: '2.00000000',
      side: 'B',
      price: '8000.1000',
      trade_id: '223',
      trade_creation_time: '2018-11-22T14:27:23.735Z'
    },
    {
      quantity: '0.50000000',
      side: 'S',
      price: '8001.3000',
      trade_id: '224',
      trade_creation_time: '2018-11-22T14:27:23.835Z'
    }
  ],
  batch_id: 778,
  type: 'update',
  channel: 'trades',
  product_id: 'BTC-USD'
}
```

## Place an order

`client.webSocket.placeOrder(order: {}): Promise<void>`

- `order: {}` - The order to place. Properties are:
  - `type: string` - `L` for limit or `M` for market.
  - `side: string` - `B` for buy or `S` for sell.
  - `productId: string` - The product id. Example: `BTC-USD`·.
  - `quantity: number` - The quantity. Example: `0.05`.
  - `price?: number` - The price. Not available for a market order. Example: `8000.1`.
  - `reference?: number` - A reference to track your order on `afr` WebSocket channel.
  - `key: {}` - A key object returned by `client.getKeys` to encrypt the order.
- returns: a promise containing nothing.

Example:

```typescript
import { Client, OrderType, OrderSide } from '@lgo/sdk';

const client = new Client(/* options */);

client.webSocket.on('open', async () => {
  const keys = await client.getKeys();
  const key = client.selectKey(keys);
  await client.webSocket.placeOrder({
    type: OrderType.limit,
    side: OrderSide.sell,
    productId: 'BTC-USD',
    quantity: 1,
    price: 50000,
    key,
    reference: Date.now()
  });
  client.webSocket.disconnect();
});

client.webSocket.connect();
```

## Cancel an order

`client.webSocket.cancelOrder(cancellation: {}): Promise<void>`

- `cancellation: {}` - The cancellation description. Properties are:
  - `orderId: string` - The id of the order to cancel.
  - `reference?: number` - A reference to track your order on `afr` WebSocket channel.
  - `key: {}` - A key object returned by `client.getKeys` to encrypt the order.
- returns: a promise containing nothing.

Example:

```typescript
const client = new Client(/* options */);

client.webSocket.on('open', async () => {
  const keys = await client.getKeys();
  const key = client.selectKey(keys);
  await client.webSocket.cancelOrder({
    orderId: '155724024975600001',
    key,
    reference: Date.now()
  });

  client.webSocket.disconnect();
});

client.webSocket.connect();
```
