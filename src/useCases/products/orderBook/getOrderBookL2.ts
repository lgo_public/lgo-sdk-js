import { AxiosInstance } from 'axios';
import ow from 'ow';

import { Configuration } from '../../../configuration';
import { createUrl } from '../../../tools';
import {
  ApiMarketDataL2,
  convertApiMarketDataL2,
  MarketDataL2
} from './orderBook';

interface Dependencies {
  configuration: Configuration;
  axios: AxiosInstance;
}

export interface GetOrderBookL2Query {
  productId: string;
}

export type GetOrderBookL2 = (
  query: GetOrderBookL2Query
) => Promise<MarketDataL2>;

export function createGetOrderBookL2(
  dependencies: Dependencies
): GetOrderBookL2 {
  const { axios, configuration } = dependencies;

  return async (query: GetOrderBookL2Query) => {
    ow(
      query,
      'query',
      ow.object.exactShape({
        productId: ow.string
      })
    );
    const { productId } = query;
    const url = createUrl(
      `${configuration.exchangeUrl}/v1/live/products/${productId}/book`,
      { level: 2 }
    );
    const { data } = await axios.get<ApiMarketDataL2>(url);
    return convertApiMarketDataL2(data);
  };
}
