export interface ApiBalances {
  balances: string[][];
}

export interface Balance {
  available: string;
  inOrder: string;
}

export interface BalancesByCurrencyCode {
  [key: string]: Balance;
}

export function convertApiBalances(apiBalances: ApiBalances): BalancesByCurrencyCode {
  const reducer = (acc: BalancesByCurrencyCode, curr: string[]) => {
    const [currencyCode, available, inOrder] = curr;
    acc[currencyCode] = {available, inOrder};
    return acc;
  };
  return apiBalances.balances.reduce(reducer, {});
}
