import { omitBy } from 'lodash/fp';
import * as querystring from 'querystring';

export function createUrl(url: string, params: any = {}): string {
  const compactedParams = omitBy(x => x === undefined || x === null, params);
  const serializedParams = querystring.stringify(compactedParams);
  return serializedParams === '' ? url : `${url}?${serializedParams}`;
}

interface DecomposedUrl {
  baseUrl: string;
  params: any;
}

export function decomposeUrl(url: string): DecomposedUrl {
  const [baseUrl, rawParams = ''] = url.split('?');
  const params = querystring.parse(rawParams);
  return { baseUrl, params };
}
