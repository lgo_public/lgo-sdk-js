import { createKey, CryptoKiMock, samples } from '../../test';
import { CreateUtcDate, CryptoKi } from '../../tools';
import {
  createPrepareOrderCancellation,
  PrepareOrderCancellation,
  validateOrderCancellation
} from './orderCancellationPreparation';
import { EncryptOrderCancellation } from './orderEncryption';

describe('Order cancellation', () => {
  let createUtcDate: CreateUtcDate;
  let prepareOrderCancellation: PrepareOrderCancellation;
  let cryptoKi: CryptoKi;
  let encryptCancelOrder: EncryptOrderCancellation;

  beforeEach(() => {
    createUtcDate = jest.fn().mockReturnValue(samples.june);
    cryptoKi = new CryptoKiMock();
    encryptCancelOrder = jest.fn().mockResolvedValue('encrypted');
    prepareOrderCancellation = createPrepareOrderCancellation({
      createUtcDate,
      cryptoKi,
      encryptOrderCancellation: encryptCancelOrder
    });
  });

  it('should encrypt and sign order', async () => {
    const key = createKey({ id: '1337' });

    const result = await prepareOrderCancellation({
      key,
      reference: 42,
      orderId: '5'
    });

    const expectedPreparedCancellation = {
      keyId: '1337',
      order: 'encrypted',
      signature: {
        source: 'RSA',
        value: 'encrypted|signed'
      },
      reference: 42
    };
    expect(result).toEqual(expectedPreparedCancellation);
  });

  it('should encrypt order using provided key and timestamp', async () => {
    const key = createKey({ id: '1337' });

    await prepareOrderCancellation({
      key,
      reference: 42,
      orderId: '5'
    });

    expect(encryptCancelOrder).toHaveBeenCalledWith(
      key,
      '5',
      Math.floor(samples.june.getTime() / 1000)
    );
  });
});

describe('Order cancellation validation', () => {
  it('should fail if order id is missing', async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderCancellation({
        key,
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `orderId` to be of type `string` but received type `undefined`'
    );
  });

  it('should fail if reference is malformed', async () => {
    const key = createKey({ id: '1337' });

    const call = () =>
      validateOrderCancellation({
        key,
        orderId: '5',
        reference: 'hello'
      } as any);

    await expect(call).toThrow(
      'Expected property `reference` to be of type `number` but received type `string`'
    );
  });

  it('should fail if key is missing', async () => {
    const call = () =>
      validateOrderCancellation({
        orderId: '5',
        reference: 42
      } as any);

    await expect(call).toThrow(
      'Expected property `key` to exist in object `cancellation`'
    );
  });
});
